/*!
    Typescript file: https://gitlab.com/steb95/forumfree-dev/-/blob/master/easyscript/typescript/customSelect.ts
 */

(function() {
    /**
     * Easyscript options.
     */
    let scriptSettings: CustomSelect.Easyscript.Options = window.FFScript.settings.script121;

    /**
     * Sentences list.
     */
    let sentences: {[key: string]: CustomSelect.Sentence.Info} = {};

    /**
     * Split string and return array of number.
     *
     * @param {string} text
     *
     * @returns {number[]}
     */
    const parseNumbers: (text: string) => number[] = (text) => {
        return text.trim().length > 0 ? text.split(',').map(elm => Number(elm.trim())).filter(elm => !isNaN(elm)) : [];
    }

    /**
     * Create options html and save sentences.
     *
     * @returns {string}
     */
    const createOptions: () => string = () => {
        let options = '',
            list: CustomSelect.Sentence.Info[] = window.Commons.utilities.uniqueItems(scriptSettings.optionList, 'title'),
            item: CustomSelect.Sentence.Info,
            sectionWhitelist: number[],
            sectionBlacklist: number[],
            topicBlacklist: number[],
            topicWhitelist: number[],
            users: number[],
            groups: number[],
            currentGroup: number = typeof window.Commons.user.group === 'string' ? 0 : window.Commons.user.group;
        for (let i = 0, size = list.length; i < size; ++i) {
            item = list[i];
            sectionWhitelist = item.secWhitelist ? parseNumbers(item.secWhitelist) : [];
            sectionBlacklist = item.secBlacklist ? parseNumbers(item.secBlacklist) : [];
            topicBlacklist = item.topicBlacklist ? parseNumbers(item.topicBlacklist): [];
            topicWhitelist = item.topicWhitelist ? parseNumbers(item.topicWhitelist): [];
            users = parseNumbers(item.customUserId);
            groups = parseNumbers(item.customGroupId);

            //If topic is in blacklist, don't do anything
            if (topicBlacklist.indexOf(window.Commons.location.topic.id) > -1) {
                continue;
            }

            if (
                (
                    //Topic whitelist check
                    topicWhitelist.indexOf(window.Commons.location.topic.id) > -1 ||
                    //Section checks
                    (
                        sectionBlacklist.indexOf(window.Commons.location.section.id) === -1 &&
                        (sectionWhitelist.indexOf(window.Commons.location.section.id) > -1 || (sectionWhitelist.length === 0 && topicWhitelist.length === 0))
                    )
                ) &&
                (
                    //User checks
                    (item.adminOnly && (window.Commons.user.isAdmin || window.Commons.user.isScriptAdmin)) ||
                    (users.indexOf(window.Commons.user.id) > -1) ||
                    (groups.indexOf(currentGroup) > -1) ||
                    (!item.adminOnly && users.length === 0 && groups.length === 0)
                ) &&
                (
                    item.title.trim().length > 0 && item.value.trim().length > 0
                )
            ) {
                item.value = window.Commons.utilities.removeJsInTags(window.Commons.utilities.removeTags(item.value));
                item.title = window.Commons.utilities.removeJsInTags(window.Commons.utilities.removeTags(item.title.replace(/"/g, '&quot;'), ['.*?']));
                options += `<option value="${(i + 1)}" data-sentence="${(i + 1)}">${item.title}</option>`;
                if (typeof item.dice === 'undefined') {
                    item.dice = false;
                }

                //Cursor position
                item.cursorStartPosition = item.value.indexOf('{{cursor}}');

                //If there is the start position
                if (item.cursorStartPosition > -1) {
                    //Check the cursor end position and remove the first placeholder length
                    item.cursorEndPosition = item.value.indexOf('{{cursor}}', (item.cursorStartPosition + 1)) - 10;

                    //Remove all cursor placeholders
                    item.value = item.value.replace(/{{cursor}}/g, '');
                } else {
                    item.cursorEndPosition = -1;
                }

                sentences['s' + (i+1)] = item;
            }
        }
        return options;
    };

    /**
     * Print select.
     *
     * @param {string} options - list of html options
     */
    const print: (options: string) => void = (options) => {
        let select = `<select class="st-custom-select" style="margin-left:10px;"><option value="" selected>${window.Commons.utilities.removeTags(scriptSettings.selectTitle, ['.*?'])}</option>${options}</select>`,
            container, appended;
        if (!window.Commons.forum.isFFMobile) {

            //Quirk layout
            if (window.Commons.forum.isQuirks) {
                container = document.querySelector('form[name="REPLIER"] select[name="FONT"]');
                if (container !== null && container.parentElement !== null) {
                    container.parentElement.insertAdjacentHTML('beforeend', select);
                }
            } else {
                //Standard layout

                //Fast send
                if (window.Commons.location.isTopic) {
                    container = document.querySelector('.fast.send .Item:first-child .left.Sub');
                    if (container !== null) {
                        container.insertAdjacentHTML('beforeend', select);
                    }
                } else if (window.Commons.location.isFullEditor) {
                    //Full editor
                    container = document.querySelector('.send .Item:first-child .left.Sub > div');
                    if (container !== null) {
                        container.insertAdjacentHTML('beforeend', select);
                    }
                }
            }
            appended = document.querySelector('select.st-custom-select');
        } else {
            //FFMobile

            container = document.querySelector('label[for="track_topic"]');
            if (container !== null) {
                container.insertAdjacentHTML('afterend', select);
            }
            appended = document.querySelector('select.st-custom-select');
        }

        //Init events
        events(appended as HTMLSelectElement);
    };

    /**
     * Select events.
     *
     * @param {HTMLSelectElement} element
     */
    const events: (element: HTMLSelectElement) => void = (element) => {
        let startPos, endPos, toInsert, sentence, value, textarea: HTMLTextAreaElement = document.querySelector('textarea#Post') as HTMLTextAreaElement;

        if (element !== null) {
            //Addclass
            if (window.Commons.forum.isFFMobile) {
                element.classList.add('forminput');
            } else {
                element.classList.add('codebuttons');
            }

            element.addEventListener('change', function() {
                sentence = sentences['s'+(this.options[this.selectedIndex].dataset.sentence)];
                element.selectedIndex = 0;

                if (typeof sentence === 'undefined') {
                    return;
                }

                value = sentence.value;

                if(value.length > 0) {
                    toInsert = value + (sentence.dice ? ' ' : '');
                    if (textarea.selectionStart || textarea.selectionStart === 0) {
                        startPos = textarea.selectionStart;
                        endPos = textarea.selectionEnd;
                        textarea.value = textarea.value.substring(0, startPos) + toInsert + textarea.value.substring(endPos, textarea.value.length);
                        textarea.focus();
                        textarea.selectionStart = startPos + (sentence.cursorStartPosition > 0 ? sentence.cursorStartPosition : toInsert.length);
                        textarea.selectionEnd = sentence.cursorEndPosition > 0 ? (startPos + sentence.cursorEndPosition) : textarea.selectionStart;
                    } else {
                        textarea.value += toInsert;
                    }
                    textarea.dispatchEvent(new Event('keyup'));
                    if (sentence.dice) {
                        let dice = (this.closest('form') as HTMLFormElement).querySelector('.dice-button');
                        if (dice !== null) {
                            dice.dispatchEvent(new Event('click'));
                        }
                    }
                }
            });

        }
    };

    /**
     * Init select.
     */
    const customSelect = () => {
        let selectOptions = createOptions();
        if (selectOptions.length > 0) {
            print(selectOptions);
        }
    };

    /**
     * Let's start!
     */
    if (window.Commons.location.isFullEditor || window.Commons.location.isTopic) {
        if (document.readyState !== 'loading') {
            customSelect();
        } else {
            document.addEventListener('DOMContentLoaded', function () {
                customSelect();
            });
        }
    }
})();
