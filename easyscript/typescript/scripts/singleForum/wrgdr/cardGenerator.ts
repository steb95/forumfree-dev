/*!
    Typescript file: https://gitlab.com/steb95/forumfree-dev/-/blob/master/easyscript/typescript/scripts/singleForum/wrgdr/cardGenerator.ts
 */
(function() {
    /**
     * Easyscript options.
     */
    let scriptSettings: wrgdrCardGenerator.Easyscript.Options = window.FFScript.settings.script162;

    //Check easyscript options
    if (scriptSettings.selector === '' || typeof scriptSettings.section === 'undefined' || scriptSettings.section < 0) {
        return;
    }

    /**
     * Get local storage item that contains card info.
     */
    const getStorage: () => { [key: string]: string } = () => {
        return JSON.parse(localStorage.getItem('gdrCardGenerator') || '{}');
    }

    /**
     * List of local storage keys used also as form's fields names.
     */
    const keyList: { [key: string]: string } = {
        'name': 'card_name',
        'avatar': 'card_avatar',
        'level': 'card_level',
        'background': 'card_background',
        'skills': 'card_skills'
    }

    //Location is the section where to create a new topic as a gdr pg card
    if (window.Commons.location.isFullEditor && window.Commons.location.section.id === scriptSettings.section) {
        //Get storage
        const storage = getStorage();

        //If storage is empty, shut down the script
        //This allows to create "regular" topics
        if (Object.keys(storage).length === 0) {
            return;
        }

        //Get topic form fields
        const titleField: HTMLInputElement = document.querySelector('input[name="TopicTitle"]') as HTMLInputElement,
            descField: HTMLInputElement = document.querySelector('input[name="TopicDesc"]') as HTMLInputElement,
            contentField: HTMLTextAreaElement = document.querySelector('textarea[name="Post"]') as HTMLTextAreaElement;

        //Set topic title
        if (storage.hasOwnProperty('card_name')) {
            titleField.value = storage['card_name'];
        }

        //Set topic description
        if (storage.hasOwnProperty('card_avatar')) {
            descField.value = `<img src="${storage['card_avatar']}" class="tmb">`;
        }

        //Set topic content
        if (scriptSettings.html) {
            const reg = new RegExp('\{\{\(' + Object.values(keyList).join('\|') + '\)\}\}', 'g');

            contentField.value = scriptSettings.html.replace(reg, function (m, s) {
                return storage.hasOwnProperty(s) ? storage[s] : '';
            });
        }

        //Delete local storage
        localStorage.removeItem('gdrCardGenerator');
    } else {
        //Get container
        const formContainer: HTMLElement | null = document.querySelector(scriptSettings.selector);

        //If container doesn't exist, shut down the script
        if (formContainer === null) {
            return;
        }

        //Set form content
        formContainer.innerHTML = `<div class="card-generator"><div class="card-generator-field"><label for="cardAvatar">Prestavolto:</label><input id="cardAvatar" class="forminput" type="text" name="${keyList.avatar}" placeholder="Prestavolto"></div>
            <div class="card-generator-field"><label for="cardName">Nome:</label><input id="cardName" class="forminput" type="text" name="${keyList.name}" placeholder="Nome"></div>
            <div class="card-generator-field"><label for="cardLevel">Livello:</label><input id="cardLevel" class="forminput" type="number" name="${keyList.level}" placeholder="Livello"></div>
            <div class="card-generator-field"><label for="cardBackground">Background:</label><textarea id="cardBackground" class="textinput" rows="8" name="${keyList.background}"></textarea></div>
            <div class="card-generator-field"><label for="cardSkills">Talenti:</label><textarea id="cardSkills" class="textinput" rows="8" name="${keyList.skills}"></textarea></div></div>
            <div class="card-save"><a class="card-button forminput" href="/?act=Post&CODE=00&f=${scriptSettings.section}">Crea!</a></div>`.replace(/(\t|\r?\n|\r)+|\s{2,}/g, '');

        //Get previous storage value
        let storage = getStorage();

        //Set events and popolate fields
        formContainer.querySelectorAll('.card-generator-field > input, .card-generator-field > textarea').forEach((field: HTMLInputElement | HTMLTextAreaElement) => {
            if (storage.hasOwnProperty(field.name)) {
                field.value = storage[field.name];
            }

            field.addEventListener('keyup', () => {
                storage[field.name] = field.value;

                localStorage.setItem('gdrCardGenerator', JSON.stringify(storage));
            });
        });
    }
})();
