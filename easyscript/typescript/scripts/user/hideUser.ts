/*!
    Typescrpt file: https://gitlab.com/steb95/forumfree-dev/-/blob/master/easyscript/typescript/hideUser.ts
 */

(async function() {
    /**
     * Easyscript options
     */
    let scriptSettings = window.FFScript.settings.script107;
    /**
     * Script version for local storage validation.
     */
    const version = '1.0.0';

    /**
     * Default value for local storage cache.
     */
    const defaultCache: HideUser.Cache.Storage = {
        'v': version,
        'exp': Date.now() + 1000 * 60 * 60 * 24,
        'users': {}
    };

    /**
     * Languages.
     */
    const i18n: {[key: string]: HideUser.Lang.Elements} = {
        'en': {
            'post': 'Hidden post, click to show.',
            'quote': 'Hidden quote, click to show',
            'topic': 'Hidden topic, click to show.',
            'message': 'Hidden message, click to show.',
            'author': 'Author:'
        },
        'it': {
            'post': 'Post nascosto, clicca per visualizzare.',
            'quote': 'Citazione nascosta, clicca per visualizzare.',
            'topic': 'Topic nascosto, clicca per visualizzare.',
            'message': 'Messaggio nascosto, clicca per visualizzare.',
            'author': 'Autore:'
        }
    };

    /**
     * User lang.
     */
    const lang: HideUser.Lang.Elements = i18n.hasOwnProperty(window.Commons.user.lang) ? i18n[window.Commons.user.lang] : i18n.en;

    /**
     * Placeholders for hidden messages.
     */
    const placeholders = {
        'post': scriptSettings.postMsg.trim().length > 0 ? window.Commons.utilities.removeJsInTags(window.Commons.utilities.removeTags(scriptSettings.postMsg, ['script', 'style', 'iframe'])) : lang.post,
        'topic': scriptSettings.topicMsg.trim().length > 0 ? window.Commons.utilities.removeJsInTags(window.Commons.utilities.removeTags(scriptSettings.topicMsg, ['script', 'style', 'iframe'])) : lang.topic,
        'message': scriptSettings.tagMsg.trim().length > 0 ? window.Commons.utilities.removeJsInTags(window.Commons.utilities.removeTags(scriptSettings.tagMsg, ['script', 'style', 'iframe'])) : lang.message,
        'quote': scriptSettings.quoteMsg && scriptSettings.quoteMsg.trim().length > 0 ? window.Commons.utilities.removeJsInTags(window.Commons.utilities.removeTags(scriptSettings.quoteMsg, ['script', 'style', 'iframe'])) : lang.quote,
        'author': lang.author
    };

    /**
     * List of accounts and groups to hide.
     */
    let toHide: {accounts: {list: {[key: string]: {info: HideUser.Cache.User; options: {author: boolean; topic: boolean; message: boolean;}}}, nick: {[key: string]: any}, id: number[]}} = {
        'accounts': {
            'list': {},
            'nick': {},
            'id': []
        }
    }

    /**
     * Users cache.
     */
    let storage: string | null = localStorage.getItem('stHideUser'), cached: HideUser.Cache.Storage;
    if (storage) {
        let now = Date.now();
        cached = JSON.parse(storage);
        if (cached.v !== version || now > cached.exp) {
            cached = defaultCache;
        }
    } else {
        cached = defaultCache;
    }

    /**
     * Get accounts info from API.
     *
     * @param {string[]} ids
     */
    const getAccountInfo: (ids: number[]) => void = async (ids) => {
        let fetched = await fetch(`/api.php?mid=${ids.join(',')}`, {
            'credentials': 'include'
        });
        if (fetched.ok) {
            let body = await fetched.json();
            delete body.idForum;
            let accounts: {id: number; nickname: string;}[] = Object.values(body);
            for (let i = 0, size = accounts.length; i < size; ++i) {
                cached.users[`u${accounts[i].id}`] = {
                    'id': accounts[i].id,
                    'name': accounts[i].nickname
                };
                toHide.accounts.list[`u${accounts[i].id}`].info = cached.users[`u${accounts[i].id}`];
                toHide.accounts.nick[cached.users[`u${accounts[i].id}`].name] = accounts[i].id.toString();
                ids.splice(ids.indexOf(accounts[i].id), 1);
            }
            //Check if there is a nonexistent user
            if (ids.length) {
                for (let i = 0, size = ids.length; i < size; ++i) {
                    cached.users[`u${ids[i]}`] = {
                        'id': ids[i],
                        'name': ''
                    };
                }
            }
        } else {
            console.warn('[st-hide-user] Api user error!');
            //Cache nonexistent users -> here if all the requested users don't exist
            for (let i = 0, size = ids.length; i < size; ++i) {
                cached.users[`u${ids[i]}`] = {
                    'id': ids[i],
                    'name': ''
                };
            }
        }
    }

    /**
     * Create placeholder HTML.
     *
     * @param {string} node
     * @param {string} type
     * @param {Object | null} author
     *
     * @returns {string}
     */
    const createPlaceholder: (node: string, type: 'post' | 'quote' | 'topic' | 'message', author?: {id: number, nick: string} | null) => string = (node, type, author = null) => {
        let opening = node === 'tr' ? `<tr class="st-hidden-${type}" style="cursor: pointer;"><td colspan="7" class="info" style="text-align: center; border-radius: 0;">` : `<${node} class="info st-hidden-${type}" style="cursor: pointer; text-align: center; border-radius: 0;">`,
            closing = node === 'tr' ? '</td></tr>' : `</${node}>`;
        let place = `${opening}${placeholders[type]}${
            author ? (` ${placeholders.author} <a href="/?act=Profile&MID=${author.id}" target="_blank">${author.nick}</a>`) : ''}${closing}`;
        return type === 'quote' ? `<div class="quote st-hidden-quote-container">${place}</div>` : place;
    }

    /**
     * Append placeholder.
     *
     * @param {HTMLElement} element
     * @param {string} type
     * @param {number} author
     * @param {boolean} first
     */
    const addPlaceholder: (element: HTMLElement, type: 'post' | 'quote' | 'topic' | 'message', author: number, first?: boolean) => void = (element, type, author, first = false) => {
        //Fix for mobile bad code -> widthtopic = $(".color").width() - 10; $("#checkorientation").text(".color * {max-width: " + widthtopic + "px}");
        if (first) {
            element.style.overflow = 'hidden';
            element.style.boxSizing = 'border-box';
            element.style.height = '0';
            element.style.opacity = '0';
            element.style.margin = '0';
            element.style.border = '0';
        } else {
            element.style.display = 'none';
        }
        element.insertAdjacentHTML(
            'beforebegin',
            createPlaceholder(
                element.nodeName.toLowerCase(),
                type,
                toHide.accounts.list[`u${author}`].options.author ? {'id': author, 'nick': toHide.accounts.list[`u${author}`].info.name} : null
            )
        );
    }

    /**
     * Parse posts/quotes and check which to hide.
     *
     * @returns {boolean} - true if at least one post/quote was hidden.
     */
    const hidePosts: () => boolean = () => {
        let posts: NodeListOf<HTMLElement> = document.querySelectorAll('.topic .post:not([data-sthideuser])'),
            post: HTMLElement,
            quotes: NodeListOf<HTMLElement>, quote: HTMLElement,
            hidden = false,
            //Fix for mobile bad code.
            mobile = window.Commons.forum.isFFMobile,
            firstCheck = document.querySelector('.topic .post[data-sthideuser]') === null;

        for (let i = 0, size = posts.length; i < size; ++i) {
            post = posts[i];
            post.dataset.sthideuser = 'post';
            let classMatch = post.classList.value.match(/box_m(\d+)/i);
            if (classMatch) {
                let author = Number(classMatch[1]);
                if (toHide.accounts.id.indexOf(author) > -1) {
                    addPlaceholder(post, 'post', author, (i === 0 && mobile && firstCheck));
                    if (!hidden) {
                        hidden = true;
                    }
                }
            }

            //Quotes
            quotes = post.querySelectorAll('.quote_top');
            for (let i = 0, size = quotes.length; i < size; ++i) {
                quote = quotes[i];
                let nickMatch = quote.innerText.match(/\((.*?)\s@/i);
                if (nickMatch) {
                    let nick = nickMatch[1];
                    if (typeof toHide.accounts.nick[nick] !== 'undefined') {
                        addPlaceholder((quote.nextElementSibling as HTMLElement), 'quote', toHide.accounts.nick[nick]);
                        if (!hidden) {
                            hidden = true;
                        }
                    }
                }
            }
        }
        return hidden;
    }

    /**
     * Parse topics and check which to hide.
     *
     * @returns {boolean}
     */
    const hideTopic: () => boolean = () => {
        let topics: NodeListOf<HTMLElement>,
            topic: HTMLElement,
            link: HTMLLinkElement,
            hidden = false;
        if (window.Commons.forum.isQuirks) {
            topics = document.querySelectorAll('.forum .xx:not([data-sthideuser])');
        } else {
            topics = document.querySelectorAll('.forum [id^="t"]:not([data-sthideuser])');
        }
        for (let i = 0, size = topics.length; i < size; ++i) {
            topic = topics[i];
            topic.dataset.sthideuser = 'topic';
            let classMatch, node;
            //Standard or FFMobile
            if (topic.nodeName === 'LI') {
                classMatch = topic.classList.value.match(/m(\d+)/i);
                node = topic;
            } else {
                //Quirks
                if (topic.firstElementChild === null || topic.firstElementChild.nodeName !== 'A') {
                    continue;
                }
                link = topic.firstElementChild as HTMLLinkElement;
                classMatch = link.classList.value.match(/user(\d+)/i);
                node = topic.parentElement as HTMLElement;
            }
            if (classMatch) {
                let author = Number(classMatch[1]);
                if (toHide.accounts.id.indexOf(author) > -1 && toHide.accounts.list[`u${author}`].options.topic) {
                    addPlaceholder(node, 'topic', author);
                    if (!hidden) {
                        hidden = true;
                    }
                }
            }
        }
        return hidden;
    }

    /**
     * Parse tag messages and check which to hide.
     *
     * @returns {boolean}
     */
    const hideMessage: () => boolean = () => {
        let messages: NodeListOf<HTMLLinkElement> = document.querySelectorAll('#tagObject .tme a[href*="/?act=Profile&MID="]:not([data-sthideuser])'),
            message: HTMLElement,
            hidden = false;
        for (let i = 0, size = messages.length; i < size; ++i) {
            message = messages[i];
            message.dataset.sthideuser = 'message';
            let classMatch = (message.getAttribute('href') as string).match(/act=Profile&MID=(\d+)/i);
            if (classMatch) {
                let author = Number(classMatch[1]);
                if (toHide.accounts.id.indexOf(author) > -1 && toHide.accounts.list[`u${author}`].options.message) {
                    addPlaceholder(message.closest('.tme') as HTMLElement, 'message', author);
                    if (!hidden) {
                        hidden = true;
                    }
                }
            }
        }
        return hidden;
    }

    /**
     * Attach event to placeholders.
     */
    const showEvent = () => {
        let hidden: NodeListOf<HTMLElement> = document.querySelectorAll('.st-hidden-post:not([data-event]), .st-hidden-quote-container:not([data-event]), .st-hidden-topic:not([data-event]), .st-hidden-message:not([data-event])');
        for (let i = 0, size = hidden.length; i < size; ++i) {
            hidden[i].dataset.event = 'true';
            hidden[i].addEventListener('click', (e) => {
                if ((e.target as HTMLElement).nodeName === 'A') {
                    return;
                }
                let toFade = hidden[i].nextElementSibling as HTMLElement, display: string;

                //Fix for FFMobile bad code
                if (toFade.style.opacity === '0' && toFade.style.overflow === 'hidden') {
                    toFade.removeAttribute('style');
                    toFade.style.display = 'none';
                }
                window.Commons.animations.fadeOut(hidden[i] as HTMLElement, 300, () => {
                    switch (toFade.nodeName) {
                        case 'LI':
                            display = 'list-item';
                            break;
                        case 'TR':
                            display = 'table-row';
                            break;
                        case 'TD':
                            display = 'table-cell';
                            break;
                        default:
                            display = 'block';
                    }
                    window.Commons.animations.fadeIn(toFade, 300, display);
                    hidden[i].remove();
                });
            });
        }
    }

    /**
     * Create an interval until document is loaded.
     *
     * @param {Function} callback
     * @param {string} type
     */
    const loadingInterval: (callback: Function, type: "post" | "topic" | "message") => void = (callback, type) => {
        if (document.readyState === 'loading') {
            let timeFunc = () => {
                if (callback()) {
                    showEvent();
                }
                if (document.readyState !== 'loading') {
                    //Remove data-attr
                    let checked: NodeListOf<HTMLElement> = document.querySelectorAll(`[data-sthideuser="${type}"]`);
                    for (let i = 0, size = checked.length; i < size; ++i) {
                        checked[i].removeAttribute('data-sthideuser');
                    }
                } else {
                    setTimeout(timeFunc, 50);
                }
            }
            setTimeout(timeFunc, 50);
        } else {
            //Remove data-attr
            let checked: NodeListOf<HTMLElement> = document.querySelectorAll(`[data-sthideuser="${type}"]`);
            for (let i = 0, size = checked.length; i < size; ++i) {
                checked[i].removeAttribute('data-sthideuser');
            }
        }
    }

    /**
     * Initiator.
     */
    const init = async () => {
        let tag = document.getElementById('tagObject') !== null;

        //If current page is not a topic neither a section and there is no tag, do nothing.
        if (!window.Commons.location.isTopic && !window.Commons.location.isSection && !tag) {
            return;
        }

        //Parse easyscript options.
        let current, uids, api: number[] = [];
        for (let i = 0, size = scriptSettings.accounts.length; i < size; ++i) {
            current = scriptSettings.accounts[i];
            uids = current.id.split(',').map(elm => Number(elm.trim())).filter(elm => !isNaN(elm));
            toHide.accounts.id = [...toHide.accounts.id, ...uids];

            for (let j = 0, jlen = uids.length; j < jlen; ++j) {
                toHide.accounts.list[`u${uids[j]}`] = {
                    'info': {
                        'id': 0,
                        'name': ''
                    },
                    'options': {
                        'author': current.seeAuthor,
                        'topic': current.hideTopics,
                        'message': current.hideTags
                    }
                }
                if (cached.users.hasOwnProperty(`u${uids[j]}`)) {
                    toHide.accounts.list[`u${uids[j]}`].info = cached.users[`u${uids[j]}`];
                    toHide.accounts.nick[cached.users[`u${uids[j]}`].name] = uids[j];
                } else {
                    if (api.indexOf(uids[j]) === -1) {
                        api.push(uids[j]);
                    }
                }
            }
        }
        if (api.length > 0) {
            await getAccountInfo(api);
        }

        //Save local storage.
        localStorage.setItem('stHideUser', JSON.stringify(cached));

        //Current page is a topic.
        if (window.Commons.location.isTopic) {
            if (hidePosts()) {
                showEvent();
            }
            loadingInterval(hidePosts, 'post');
        } else if (window.Commons.location.isSection) {
            //Current page is a section.
            if (hideTopic()) {
                showEvent();
            }
            loadingInterval(hideTopic, 'topic');
        }

        //Tag
        if (tag) {
            if (hideMessage()) {
                showEvent();
            }
            loadingInterval(hideMessage, 'message');
        }
    }

    //Let's start!
    await init();
})();
