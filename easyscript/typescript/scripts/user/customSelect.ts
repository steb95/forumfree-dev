/*!
    Typescript file: https://gitlab.com/steb95/forumfree-dev/-/blob/master/easyscript/typescript/userCustomSelect.ts
 */

(function() {
    /**
     * Easyscript options.
     */
    let scriptSettings: UserCustomSelect.Easyscript.Options = window.FFScript.settings.script122;

    /**
     * Sentences list.
     */
    let sentences: {[key: string]: UserCustomSelect.Sentence.Info} = {};

    /**
     * Forum subdomain.
     */
    const SUBDOMAIN: string = document.domain.split('.')[0];

    /**
     * Current domain.
     */
    const DOMAIN: string = (document.domain.match(/(forumfree.it|forumcommunity.net|blogfree.net)$/i) as RegExpMatchArray)[1];

    /**
     * Short domain.
     */
    const SHORT_DOMAIN: string = (DOMAIN === 'forumfree.it' ? 'ff' : (DOMAIN === 'forumcommunity.net' ? 'fc' : 'bf'));

    /**
     * Init select.
     */
    const customSelect: () => void = () => {
        let selectOptions = createOptions();
        if (selectOptions.length > 0) {
            print(selectOptions);
        }
    };

    /**
     * Create options html and save sentences.
     */
    const createOptions: () => string = () => {
        let options = '',
            list: UserCustomSelect.Sentence.Info[] = window.Commons.utilities.uniqueItems(scriptSettings.optionList, 'title'),
            item;
        for (let i = 0, size = list.length; i < size; ++i) {
            item = list[i];

            //Set forum list
            item.forumList = (typeof item.forum === 'undefined' || item.forum.length === 0) ? [] : item.forum.toLowerCase().split(',').map((single: string) => {
                return single.indexOf('.') > -1 ? single.split('.')[0] : single;
            });

            //Set item post join
            if (typeof item.post !== 'undefined' && item.post.length > 0) {
                item.postInfo = parsePost(item.post);
                item.hasPost = item.postInfo.id > 0;
            } else {
                item.hasPost = false;
            }

            //Check conditions
            if (
                item.title.length > 0
                && (item.value.length > 0 || item.hasPost)
                && (item.forumList.length === 0 || item.forumList.indexOf(SUBDOMAIN) > -1)
            ) {
                if (item.hasPost) {
                    item.value = '';
                    item.cursorStartPosition = item.cursorEndPosition = -1;
                } else {
                    item.value = window.Commons.utilities.removeTags(item.value);

                    //Check cursor position
                    item.cursorStartPosition = item.value.indexOf('{{cursor}}');

                    //If there is the start position
                    if (item.cursorStartPosition > -1) {
                        //Check the cursor end position and remove the first placeholder length
                        item.cursorEndPosition = item.value.indexOf('{{cursor}}', (item.cursorStartPosition + 1)) - 10;

                        //Remove all cursor placeholders
                        item.value = item.value.replace(/{{cursor}}/g, '');
                    } else {
                        item.cursorEndPosition = -1;
                    }
                }
                item.title = window.Commons.utilities.removeTags(item.title.replace(/"/g, '&quot;'));
                options += `<option value="${(i + 1)}" data-sentence="${(i + 1)}"${item.hasPost ? ' data-post="true"' : ''}>${item.title}</option>`;

                //Fix for older versions that don't have item.dice property
                if (typeof item.dice === 'undefined') {
                    item.dice = false;
                }

                //Save sentence
                sentences['s' + (i+1)] = item;
            }
        }
        return options;
    };

    /**
     * Parse post url.
     */
    const parsePost: (post: string) => UserCustomSelect.Post.Info = (post) => {
        post = post.toLowerCase();
        let reg = new RegExp(`^https:\\/\\/([^.]+)\\.${DOMAIN.replace('.', '\\.')}.*#entry(\\d+)$`, 'i');
        let match = post.match(reg);
        if (match === null || match.length < 3) {
            return {
                'subdomain': '',
                'id': 0,
                'sent': false,
                'content': ''
            };
        }
        return {
            'subdomain': match[1],
            'id': Number(match[2]),
            'sent': false,
            'content': ''
        };
    };

    /**
     * Print select.
     */
    const print: (options: string) => void = (options) => {
        let select = `<select class="st-user-custom-select" style="margin-left:10px;"><option value="" selected>${window.Commons.utilities.removeTags(scriptSettings.selectTitle, ['.*?'])}</option>${options}</select>`,
            container,
            appended: HTMLSelectElement|null;
        if (!window.Commons.forum.isFFMobile) {

            //Quirk layout
            if (window.Commons.forum.isQuirks) {
                container = document.querySelector('form[name="REPLIER"] select[name="FONT"]');
                if (container !== null) {
                    (container.parentElement as HTMLElement).insertAdjacentHTML('beforeend', select);
                }
            } else {
                //Standard layout

                //Fast send
                if (window.Commons.location.isTopic) {
                    container = document.querySelector('.fast.send .Item:first-child .left.Sub');
                    if (container !== null) {
                        container.insertAdjacentHTML('beforeend', select);
                    }
                } else if (window.Commons.location.isFullEditor) {
                    //Full editor
                    container = document.querySelector('.send .Item:first-child .left.Sub > div');
                    if (container !== null) {
                        container.insertAdjacentHTML('beforeend', select);
                    }
                }
            }
            appended = document.querySelector('select.st-user-custom-select');
        } else {
            //FFMobile

            container = document.querySelector('label[for="track_topic"]');
            if (container !== null) {
                container.insertAdjacentHTML('afterend', select);
            }
            appended = document.querySelector('select.st-user-custom-select');
        }

        //Init events
        events(appended);
    };

    /**
     * Select events.
     */
    const events: (element: HTMLSelectElement|null) => void = (element) => {
        let startPos, endPos, toInsert, sentence, value,
            textarea: HTMLTextAreaElement = document.querySelector('textarea#Post') as HTMLTextAreaElement;

        if (element !== null) {
            //Addclass
            if (window.Commons.forum.isFFMobile) {
                element.classList.add('forminput');
            } else {
                element.classList.add('codebuttons');
            }

            element.addEventListener('change', async function() {
                sentence = sentences['s'+(this.options[this.selectedIndex].dataset.sentence)];

                element.selectedIndex = 0;

                if (typeof sentence === 'undefined') {
                    return;
                }
                value = sentence.value;

                if(value.length > 0 || sentence.hasPost) {
                    if (sentence.hasPost) {
                        if (sentence.postInfo.sent) {
                            toInsert = sentence.postInfo.content;
                        } else {
                            toInsert = await fetchContent(sentence.postInfo);
                            sentence.postInfo.sent = true;
                            sentence.postInfo.content = toInsert;
                        }
                    } else {
                        toInsert = value;
                    }
                    toInsert += (sentence.dice ? ' ' : '');
                    if (textarea.selectionStart || textarea.selectionStart === 0) {
                        startPos = textarea.selectionStart;
                        endPos = textarea.selectionEnd;
                        textarea.value = textarea.value.substring(0, startPos) + toInsert + textarea.value.substring(endPos, textarea.value.length);
                        textarea.focus();
                        textarea.selectionStart = startPos + (sentence.cursorStartPosition > 0 ? sentence.cursorStartPosition : toInsert.length);
                        textarea.selectionEnd = sentence.cursorEndPosition > 0 ? (startPos + sentence.cursorEndPosition) : textarea.selectionStart;
                    } else {
                        textarea.value += toInsert;
                    }
                    textarea.dispatchEvent(new Event('keyup'));
                    if (sentence.dice) {
                        let dice = (this.closest('form') as HTMLFormElement).querySelector('.dice-button');
                        if (dice !== null) {
                            dice.dispatchEvent(new Event('click'));
                        }
                    }
                }
            });

        }
    };

    /**
     * Fetch post content.
     */
    const fetchContent: (post: UserCustomSelect.Post.Info) => Promise<string> = async (post) => {
        let fetched = await fetch(`https://${post.subdomain}.${DOMAIN}/api.php?cookie=1&p=${post.id}`, {
            'credentials': 'include'
        }).catch((error: Error) => error);
        if (fetched instanceof Error) {
            console.warn('[user-custom-select] Error while fatching custom phrase: ' + fetched.message);
            throwToast('Errore durante l\'invio della richiesta GET.', 'error');
            return '';
        }
        if (!fetched.ok) {
            console.warn('[user-custom-select] Error: API responded with a status of ' + fetched.status);
            throwToast('Errore durante l\'acquisizione del contenuto, probabilmente hai inserito un url non valido.<br>Status: ' + fetched.status, 'error');
            return '';
        }
        let json = await fetched.json().catch((error: Error) => error);
        if (json instanceof Error) {
            console.warn('[user-custom-select] Error while parsing API reponse as JSON: ' + json.message, 'error');
            throwToast('Errore durante la decodifica della risposta dell\'API.');
            return '';
        }
        if (typeof json['p' + post.id] === 'undefined') {
            console.warn('[user-custom-select] Error: you probably don\'t have access to this post', post);
            throwToast('Il post sembra essere vuoto, probabilmente non vi hai accesso.');
            return '';
        }
        return decodeHTML(parseContent(json['p' + post.id].content));
    };

    /**
     * Parse API post content.
     */
    const parseContent: (content: string) => string = (content) => {
        if (
            /javascript:spoiler\(\)/.test(content)
            || /<div class="code_top" align="left"><b>CODICE<\/b><\/div>/.test(content)
            || /<div id="diceRolled-\d+"/.test(content)
        ) {
            content = parseElements(content);
        }

        return content
            //Emoticons
            .replace(/<img src="[^"]+" alt="(:[^:]+:)">/gi, '$1')
            //Edit span
            .replace(/<br><br><span class="edit".*?<\/span>/i, '')
            //Line break
            .replace(/<br>/gi, '\n');
    };

    /**
     * Parse spoiler and code blocks.
     *
     * Remove dice blocks.
     */
    const parseElements: (text: string) => string = (text) => {
        let div = document.createElement('DIV');
        div.innerHTML = text;

        //Spoiler
        div.querySelectorAll('.spoiler').forEach(elm => {
            elm.outerHTML = `[SPOILER]${(elm.querySelector('.code') as HTMLElement).innerHTML}[/SPOILER]`;
        });

        //Code
        div.querySelectorAll('div[align="center"] > .code_top').forEach(elm => {
            (elm.parentElement as HTMLElement).outerHTML = `[CODE]${((elm.parentElement as HTMLElement).querySelector('.code') as HTMLElement).innerHTML}[/CODE]`;
        });

        //Dice
        div.querySelectorAll('div[id^="diceRolled-"]').forEach(elm => elm.remove());
        return div.innerHTML;
    };

    /**
     * Decode HTML entities in API response.
     */
    const decodeHTML: (text: string) => string = (text) => {
        let textArea = document.createElement('textarea');
        textArea.innerHTML = text;
        return textArea.value;
    };

    /**
     * Throw a toast
     */
    const throwToast: (content: string, type?: string) => void = (content, type = 'info') => {
        window.Commons.toast.show({
            'class': (type === 'error' ? 'cs-toast-error' : 'cs-toast-info'),
            'title': 'Frasi predefinite!',
            'content': content + `<br><br><div>Link utili: <a href="https://ffboard.forumfree.it/?t=76607458" target="_blank">Guida</a> - <a href="https://ffboard.forumfree.it/?act=Post&CODE=00&f=64397043" target="_blank">Supporto</a> - <a href="https://ffboard.forumfree.it/?pag=easyscript&domain=${SHORT_DOMAIN + window.Commons.user.id}&evd=122&s_tab=ffb" target="_blank">Script</a>`,
            'ttl': 30 * 1000
        });
    };

    //Start!
    if (document.readyState !== 'loading') {
        customSelect();
    } else {
        document.addEventListener('DOMContentLoaded', function() {
            customSelect();
        });
    }

})();
