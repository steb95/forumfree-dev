(function() {
    //Declare utilityService
    const utilityService = function(): Commons.Utility.Service {
        if (this._utilityService) {
            return this._utilityService;
        }

        /**
         * Filter input array and return items that have distinct value of the key property
         *
         * @param {object[]} array
         * @param {string} key
         *
         * @returns {object[]}
         */
        const uniqueItems = (array: {[key: string]: any}[], key: string): object[] => {
            let flags: boolean[] = [],
                output: object[] = [];
            for (let i = 0, size = array.length; i < size; i++) {
                if (flags[array[i][key]]) {
                    continue;
                }
                flags[array[i][key]] = true;
                output.push(array[i]);
            }
            return output;
        }

        /**
         * Get parameter from url
         *
         * @param {string} name
         * @param {string|null} url
         *
         * @returns {string|null}
         */
        const getUrlParameter = (name: string, url: string | null = null): string | null => {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            let regex: RegExp = new RegExp('[\\?&]' + name + '=([^&#]*)');
            let results: RegExpExecArray | null = regex.exec(url !== null ? url : window.location.search);
            return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        /**
         * Get cookie from name
         *
         * @param {string} name
         *
         * @returns {string|null}
         */
        const getCookie = (name: string): string | null => {
            let cookie: string = document.cookie,
                match: RegExpMatchArray | null,
                result: string | null = null,
                reg: RegExp = new RegExp(name + '=([^;]+)', 'i');

            match = cookie.match(reg);

            if (match) {
                result = match[1];
            }

            return result;
        }

        /**
         * Get all cookies as key => val object
         *
         * @returns {object}
         */
        const getAllCookies = (): object => {
            let cookie: string = document.cookie,
                match: RegExpMatchArray | null,
                explode: string[],
                result: {[key: string]: any} = {};

            match = cookie.match(/[^ ;]+=[^;]+/gi);

            if (match) {
                for (let i = 0; i < match.length; i++) {
                    explode = match[i].split('=');
                    result[explode[0]] = explode[1];
                }
            }

            return result;
        }

        /**
         * Set a new cookie
         *
         * @param {string} name
         * @param {string} value
         * @param {?number} seconds
         * @param {boolean} network
         *
         * @returns {boolean}
         */
        const setCookie = (name: string, value: string, seconds?: number, network: boolean = false): boolean => {
            if (name.length === 0 || value.length === 0) {
                return false;
            }

            try {
                let expires: string = '',
                    domain: string = '';

                if (typeof seconds === 'number') {
                    let date = new Date();
                    date.setSeconds(date.getSeconds() + seconds);
                    expires = ';expires=' + date.toUTCString();
                }

                if (network) {
                    let match: RegExpMatchArray | null = window.location.host.match(/(forumfree|forumcommunity|blogfree)\.(it|net)$/gi);
                    if (match) {
                        domain = ';domain=' + match[0];
                    }
                }
                document.cookie = name + "=" + value + expires + domain + ';Secure;SameSite=Lax';
                return true;
            } catch (e) {
                console.warn(`[Commons] Error while setting cookie. Name: ${name}, value: ${value}, seconds: ${seconds}, network: ${network}`);
                return false;
            }
        }

        /**
         * Remove tags from string
         *
         * @param {string} text
         * @param {string[]} tags
         *
         * @returns {string}
         */
        const removeTags = (text: string, tags: string[] = ['script', 'style']): string => {
            let reg = new RegExp('<\(/\?\(' + tags.join('\|') + '\)\[\^>\]\*>\)', 'gi');
            return text.replace(reg, '&lt;$1');
        }

        /**
         * Remove js in tags (e.g. onclick=)
         *
         * @param {string} text
         *
         * @returns {string}
         */
        const removeJsInTags = (text: string): string => {
            return text.replace(/<([^>]*(on\w+(\s+)?=(.*?)|javascript)[^>]*>)/gi, '&lt;$1');
        }

        /**
         * Format a date following input replacer
         *
         * @param {number| string} value
         * @param {string} format
         *
         * @returns {string|null}
         */
        const dateFormat = (value: number | string, format: string = 'Y-m-d H:i:s'): string | null => {
            try {
                let date = new Date(value),
                    symbols: {[key: string]: any} = {
                        'Y': date.getFullYear(),
                        'n': date.getMonth() + 1,
                        'j': date.getDate(),
                        'G': date.getHours(),
                        'i': date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes(),
                        's': date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
                    };
                symbols['m'] = symbols.n < 10 ? '0' + symbols.n : symbols.n;
                symbols['d'] = symbols.j < 10 ? '0' + symbols.j : symbols.j;
                symbols['H'] = symbols.G < 10 ? '0' + symbols.G : symbols.G;
                symbols['y'] = symbols.Y.toString().substr(-2);
                let reg = new RegExp(Object.keys(symbols).join('\|'), 'g');
                return format.replace(reg, function(r) {
                    return symbols[r];
                });
            } catch (e) {
                console.warn(`[Commons] Error while formatting date. Value: ${value}, format: ${format}`);
                return null;
            }
        }

        const bbcodeParser: any = function() {
            if (!(this instanceof bbcodeParser)) {
                return new bbcodeParser();
            }

            let _codes: {regexp: RegExp, replacement: string}[] = [];

            this.set = (codes: {[key: string]: any}, append = false): void => {
                let current;
                current = Object.keys(codes).map(function(regex: string) {
                    const replacement = codes[regex];
                    return {
                        'regexp': new RegExp(regex, 'igm'),
                        'replacement': replacement
                    };
                }, this);
                if (append) {
                    _codes = _codes.concat(current);
                } else {
                    _codes = current;
                }
            }

            this.parse = (text: string): string => {
                let tokens: string[] = [];
                _codes.forEach((code, idx) => {
                    tokens[idx] = "-=-=-cod" + idx + "-=-=-"; // generate unique string?
                });

                function escapeRegExp(string: string) {
                    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
                }

                _codes.forEach((code, idx) => {
                    text = text
                        .replace(code.regexp, code.replacement)
                        .replace(new RegExp(escapeRegExp(code.replacement), 'gm'), tokens[idx]);
                });

                _codes.forEach((code, idx) => {
                    text = text
                        .replace(new RegExp(escapeRegExp(tokens[idx]), 'gm'), code.replacement);
                });

                return text;
            }
        }

        this._utilityService = {
            'uniqueItems': uniqueItems,
            'getUrlParameter': getUrlParameter,
            'getCookie': getCookie,
            'getAllCookies': getAllCookies,
            'setCookie': setCookie,
            'removeTags': removeTags,
            'removeJsInTags': removeJsInTags,
            'dateFormat': dateFormat,
            'bbcodeParser': bbcodeParser
        }

        return this._utilityService;
    }

    //Declare forum service
    const forumService = function(): Commons.Forum.Service {
        if (this._forumService) {
            return this._forumService;
        }

        /**
         * FF Layout
         *
         * @type {number}
         */
        const layout: number = typeof ff_layout === 'undefined' ? 0 : ff_layout;

        /**
         * Current layout is FFMobile
         *
         * @type {boolean}
         */
        const isFFMobile: boolean = layout === 0;

        /**
         * Current layout is Quirks
         *
         * @type {boolean}
         */
        const isQuirks: boolean = layout === 1;

        /**
         * Current layout is Standard
         *
         * @type {boolean}
         */
        const isStandard: boolean = layout === 2;

        /**
         * Current layout is Responsive
         *
         * @type {boolean}
         */
        const isResponsive: boolean = layout === 3;

        this._forumService = {
            /**
             * Return forum id
             *
             * @type {number}
             */
            get id(): number {
                delete this.id;
                return this.id = typeof ff_cid !== 'undefined' ? ff_cid : 0;
            },
            'layout': layout,
            'isFFMobile': isFFMobile,
            'isQuirks': isQuirks,
            'isStandard': isStandard,
            'isResponsive': isResponsive
        }

        return this._forumService;
    }

    //Declare location service
    const locationService = function(forum: Commons.Forum.Service, utilities: Commons.Utility.Service): Commons.Location.Service {
        if (this._locationService) {
            return this._locationService;
        }

        this._locationService = {
            /**
             * Check if current page is homepage. window.location.search matches homepage with blog
             *
             * @type {boolean}
             */
            get isHome(): boolean {
                delete this.isHome;
                return this.isHome = document.body.id === 'board' || window.location.search === '';
            },

            /**
             * Check if current page is blog
             *
             * @type {boolean}
             */
            get isBlog(): boolean {
                delete this.isBlog;

                let isBlog: boolean = false;

                //From page 2 is no more a blog
                if (forum.isFFMobile) {
                    isBlog = document.querySelector('.list-group.article') !== null && (!this.isTopic || utilities.getUrlParameter('st') === null);
                } else if (forum.isStandard) {
                    isBlog = document.body.id === 'blog' && (!this.isTopic || utilities.getUrlParameter('st') === null);
                } else if (forum.isQuirks) {
                    //Body class matches article list (section page), the remaining part matches topic page
                    isBlog = document.body.classList.contains('blog') || document.querySelector('a[name="comments"]') !== null;
                }

                return this.isBlog = isBlog;
            },

            /**
             * Check if current page is an article
             *
             * @returns {boolean}
             */
            get isArticle(): boolean {
                delete this.isArticle;

                let isArticle: boolean = false;

                if (this.isTopic) {
                    if (forum.isFFMobile) {
                        isArticle = document.querySelector('.list-group.topic.article') !== null;
                    } else if (forum.isStandard) {
                        isArticle = document.body.id === 'blog';
                    } else if (forum.isQuirks) {
                        //There are no elements for pages > 1
                        isArticle = document.querySelector('a[name="comments"]') !== null;
                    }
                }

                return this.isArticle = isArticle;
            },

            /**
             * Check if current page is list of articles
             *
             * @returns {boolean}
             */
            get isArticleList(): boolean {
                delete this.isArticleList;

                let isArticleList: boolean = false;

                if (!this.isTopic) {
                    if (forum.isStandard) {
                        isArticleList = document.body.id === 'blog' && !this.isTopic;
                    } else if (forum.isFFMobile) {
                        isArticleList = document.querySelector('.list-group.article:not(.topic)') !== null;
                    } else if (forum.isQuirks) {
                        isArticleList = document.body.classList.contains('blog');
                    }
                }

                return this.isArticleList = isArticleList;
            },

            /**
             * Check if current page is section. getUrlParameter matches section blog (but not homepage)
             *
             * @type {boolean}
             */
            get isSection(): boolean {
                delete this.isSection;
                return this.isSection = document.body.id === 'forum' || (utilities.getUrlParameter('f') !== null && utilities.getUrlParameter('act') === null);
            },

            /**
             * Check if current page is topic. regex matches blog topic
             *
             * @type {boolean}
             */
            get isTopic(): boolean {
                delete this.isTopic;

                let isTopic = document.body.id === 'topic' || /t([0-9]+)/.test(document.body.className);

                if (forum.isQuirks && !isTopic) {
                    isTopic = utilities.getUrlParameter('t') !== null && utilities.getUrlParameter('act') === null;
                }

                return this.isTopic = isTopic;
            },

            /**
             * Check if current page is full editor reply
             *
             * @type {boolean}
             */
            get isFullEditor(): boolean {
                delete this.isFullEditor;

                let isFullEditor = document.body.id === 'send';

                if (forum.isQuirks && !isFullEditor) {
                    isFullEditor = utilities.getUrlParameter('act') === 'Post';
                }

                return this.isFullEditor = isFullEditor;
            },

            /**
             * Check if current page is profile
             *
             * @type {boolean}
             */
            get isProfile(): boolean {
                delete this.isProfile;

                let isProfile = document.body.id === 'profile';

                if (forum.isQuirks && !isProfile) {
                    isProfile = utilities.getUrlParameter('act') === 'Profile';
                }

                return this.isProfile = isProfile;
            },

            'section': {
                /**
                 * Section id
                 *
                 * @type {number}
                 */
                get id(): number {
                    delete this.id;

                    let id: number;

                    //Check body class
                    let bodyClass: RegExpMatchArray | null = document.body.className.match(/(?:\s|^)f([0-9]+)(?:\s|$)/i);
                    if (bodyClass) {
                        id = Number(bodyClass[1]);

                        if (!isNaN(id)) {
                            return this.id = id;
                        }
                    }

                    //Check url param
                    let urlParam: string | null = utilities.getUrlParameter('f');
                    if (urlParam) {
                        id = Number(urlParam);

                        if (!isNaN(id)) {
                            return this.id = id;
                        }
                    }

                    //Check nav link
                    let navUrl: HTMLLinkElement | null = document.querySelector('.nav a[href*="?f="]');
                    if (navUrl) {
                        let urlMatch = navUrl.href.match(/[?&]f=([0-9]+)/i);
                        id = urlMatch !== null ? Number(urlMatch[1]) : 0;

                        if (!isNaN(id)) {
                            return this.id = id;
                        }
                    }

                    //Not found
                    return this.id = 0;
                },

                /**
                 * Section title
                 *
                 * @type {string|null}
                 */
                get title(): string | null {
                    delete this.title;

                    let title: string | null = null;

                    if (this.id > 0) {
                        //Check mtitle
                        let mtitleSez: HTMLElement | null = document.querySelector('.forum .mback .mtitle, .forum .top .title');
                        if (mtitleSez) {
                            let mtitleh1: HTMLElement | null = mtitleSez.querySelector('h1, strong');
                            title = mtitleh1 ? mtitleh1.innerHTML : mtitleSez.innerText;
                        } else {
                            //FFMobile layout
                            if (forum.isFFMobile) {
                                //.section-title is for topic, #nav-title for section (but it needs to check section-title before)
                                let elm: HTMLElement | null = document.querySelector('.section-title');
                                if (elm) {
                                    title = elm.innerText;
                                }
                            } else {
                                //Standard and Quirks layout
                                let currentForum: HTMLLinkElement | null = document.querySelector('.nav li:last-of-type');
                                if (currentForum) {
                                    let link: HTMLLinkElement | null = currentForum.querySelector('a[href*="?f="]');

                                    if (link) {
                                        title = link.innerText;
                                    } else {
                                        title = currentForum.innerText.trim();
                                    }
                                } else {
                                    //Check Feed rss -> article in blog section as homepage
                                    currentForum = document.querySelector('.menuwrap .rss li:last-of-type a');
                                    if (currentForum) {
                                        let splitted: string[] = currentForum.innerText.split('-');
                                        if (splitted.length > 1) {
                                            title = splitted[splitted.length - 1].trim();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return this.title = title;
                }
            },
            'topic': {
                /**
                 * Topic id
                 *
                 * @type {number}
                 */
                get id(): number {
                    delete this.id;

                    let id: number;

                    //Check body class
                    let bodyClass = document.body.className.match(/(?:\s|^)t([0-9]+)(?:\s|$)/i);
                    if (bodyClass) {
                        id = Number(bodyClass[1]);

                        if (!isNaN(id)) {
                            return this.id = id;
                        }
                    }

                    //Check url param
                    let urlParam: string | null = utilities.getUrlParameter('t');
                    if (urlParam) {
                        id = Number(urlParam);

                        if (!isNaN(id)) {
                            return this.id = id;
                        }
                    }

                    //Not found
                    return this.id = 0;
                },

                /**
                 * Topic title
                 *
                 * @type {string|null}
                 */
                get title(): string | null {
                    delete this.title;

                    let title = null;

                    if (this.id > 0) {
                        //Check mtitle
                        let mtitleTopic: HTMLElement | null = document.querySelector('h2.btitle, .topic .mback .mtitle, .topic .top .title');
                        if (mtitleTopic) {
                            let mtitleh1: HTMLElement | null = mtitleTopic.querySelector('h1, strong');

                            if (mtitleh1) {
                                title = mtitleh1.innerHTML;
                            } else {
                                title = mtitleTopic.innerText;
                            }
                        }
                    }

                    return this.title = title;
                }
            }
        }

        return this._locationService;
    }

    //Declare user service
    const userService = function(forum: Commons.Forum.Service): Commons.User.Service {
        if (this._userService) {
            return this._userService;
        }

        this._userService = {
            /**
             * Get user id
             *
             * @returns {number}
             */
            get id(): number {
                delete this.id

                let id: number = 0;

                if (typeof ff_mid !== 'undefined') {
                    id = ff_mid;
                } else if (forum.isFFMobile) {
                    let profileLink: HTMLLinkElement | null = document.querySelector('aside#Left .nickname');
                    if (profileLink) {
                        id = Number(profileLink.href.split('Profile&MID=')[1]);
                    }
                } else if (forum.isStandard || forum.isQuirks) {
                    let profileLink: HTMLLinkElement | null = document.querySelector('.menuwrap > ul:nth-of-type(1) .menu > a');
                    if (profileLink) {
                        id = Number(profileLink.href.split('Profile&MID=')[1]);
                    }
                }

                return this.id = id;
            },

            /**
             * Get user avatar url
             *
             * @returns {string|null}
             */
            get avatar(): string | null {
                delete this.avatar;
                if (!this.isGuest) {
                    let avatar: HTMLImageElement | null = document.querySelector('aside#Left.sidebar .user_details .avatar img, .menuwrap li:first-child .avatar img');
                    if (avatar) {
                        return this.avatar = avatar.src;
                    }
                }

                return this.avatar = null;
            },

            /**
             * Get user nickname, null if guest
             *
             * @returns {string|null}
             */
            get nickname(): string | null {
                delete this.nickname;
                if (!this.isGuest) {
                    let nickname: HTMLElement | null = document.querySelector('aside#Left.sidebar .user_details .nickname, .menuwrap li:first-child .nick, .menuwrap li:first-child #nick');
                    if (nickname) {
                        return this.nickname = nickname.innerText;
                    }
                }

                return this.nickname = null;
            },

            /**
             * Get user group, -1 if guest
             *
             * @returns {number|string}
             */
            get group(): number | string {
                delete this.group;

                if (this.isGuest) {
                    return this.group = -1;
                }

                let group: number | string = 0;

                //Remove fake group class in group page
                let bodyClass = document.body.id === 'group' ? document.body.className.substring(3) : document.body.className;

                let bodyMatch: RegExpMatchArray | null = bodyClass.match(/(?:\s|^)g([0-9]+)(?:\s|$)/i);
                if (bodyMatch) {
                    let found: number = Number(bodyMatch[1]);
                    if (!isNaN(found)) {
                        group = found;
                    }
                } else {
                    //Check admin or mod group
                    bodyMatch = document.body.className.match(/(?:\s|^)(admin|mod_sez)(?:\s|$)/i);
                    if (bodyMatch) {
                        group = bodyMatch[1];
                    }
                }

                return this.group = group;
            },

            get lang(): string {
                delete this.lang;
                let searchLang: RegExpMatchArray | null = document.body.className.match(/(?:\s|^)[A-z]{2}(?:\s|$)/gi),
                    lang = 'en';

                if (searchLang) {
                    lang = searchLang[searchLang.length - 1].trim();
                }

                return this.lang = lang;
            },

            /**
             * Check if user is admin
             *
             * @returns {boolean}
             */
            get isAdmin(): boolean {
                delete this.isAdmin;
                return this.isAdmin = document.body.classList.contains('admin');
            },

            /**
             * Check if user is script admin
             *
             * @returns {boolean}
             */
            get isScriptAdmin(): boolean {
                delete this.isScriptAdmin;
                return this.isScriptAdmin = typeof window.FFScript !== 'undefined' && typeof window.FFScript.scripts_admin !== 'undefined' && window.FFScript.scripts_admin.indexOf((this.id as never)) > -1;
            },

            /**
             * Check if user is mod
             *
             * @returns {boolean}
             */
            get isMod(): boolean {
                delete this.isMod;
                return this.isMod = document.body.classList.contains('mod_sez');
            },

            /**
             * Check if user is global mod
             *
             * @returns {boolean}
             */
            get isGlobalMod(): boolean {
                delete this.isGlobalMod;
                return this.isGlobalMod = document.body.classList.contains('globalmod');
            },

            /**
             * Check if user is graphic admin
             *
             * @returns {boolean}
             */
            get isGraphicAdmin(): boolean {
                delete this.isGraphicAdmin;
                return this.isGraphicAdmin = document.body.classList.contains('admin_graphic');
            },

            /**
             * Check if user is user admin
             *
             * @returns {boolean}
             */
            get isUserAdmin(): boolean {
                delete this.isUserAdmin;
                return this.isUserAdmin = document.body.classList.contains('admin_user');
            },

            /**
             * Check if user is section admin
             *
             * @returns {boolean}
             */
            get isSectionAdmin(): boolean {
                delete this.isSectionAdmin;
                return this.isSectionAdmin = document.body.classList.contains('admin_sez');
            },

            /**
             * Check if user is guest
             *
             * @type {boolean}
             */
            get isGuest(): boolean {
                delete this.isGuest;
                return this.isGuest = document.body.classList.contains('guest');
            }
        }

        return this._userService;
    }

    //Declare device service
    const deviceService = function(): Commons.Device.Service {
        if (this._deviceService) {
            return this._deviceService;
        }

        this._deviceService = {
            /**
             * Check if device is touch
             *
             * @returns {boolean}
             */
            get isTouch(): boolean {
                delete this.isTouch;

                if (('ontouchstart' in window)) {
                    return this.isTouch = true;
                }
                let prefixes: string[] = ' -webkit- -moz- -o- -ms- '.split(' '),
                    mq = function (query: string): boolean {
                        return window.matchMedia(query).matches;
                    };
                let query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');

                return this.isTouch = mq(query);
            }
        }

        return this._deviceService;
    }

    //Declare animation service
    const animationService = function(): Commons.Animation.Service {
        if (this._animationService) {
            return this._animationService;
        }

        /**
         * Fade in an element
         *
         * @param {HTMLElement} element
         * @param {number} duration - duration in milliseconds
         * @param {string} display
         * @param {function} callback
         */
        const fadeIn = (element: HTMLElement, duration: number = 300, display: string = 'block', callback: (() => any) | null = null): void => {
            if (element.style.display.length > 0) {
                if (element.style.display !== 'none') {
                    return;
                }
            } else if (getComputedStyle(element).display !== 'none') {
                return;
            }

            const startTime: number = performance.now();
            element.style.opacity = '0';
            element.style.display = display;


            function _fade(timestamp: number) {
                const elapsed: number = timestamp - startTime;
                const step: number = Math.min(elapsed / duration, 1);
                element.style.opacity = step.toString();
                if (step < 1) {
                    requestAnimationFrame(_fade);
                } else {
                    if (callback) {
                        callback();
                    }
                }
            }

            requestAnimationFrame(_fade);
        };

        /**
         * Fade out an element
         *
         * @param {HTMLElement} element
         * @param {number} duration - duration in milliseconds
         * @param {function} callback
         */
        const fadeOut = (element: HTMLElement, duration: number = 300, callback: (() => any) | null = null): void => {
            if (element.style.display.length > 0) {
                if (element.style.display === 'none') {
                    return;
                }
            } else if (getComputedStyle(element).display === 'none') {
                return;
            }

            const startTime: number = performance.now();
            element.style.opacity = '1';

            function _fade(timestamp: number) {
                const elapsed: number = timestamp - startTime;
                const step: number = Math.max(1 - (elapsed / duration), 0);
                element.style.opacity = step.toString();
                if (step > 0) {
                    requestAnimationFrame(_fade);
                } else {
                    element.style.display = 'none';
                    if (callback) {
                        callback();
                    }
                }
            }

            requestAnimationFrame(_fade);
        };

        this._animationService = {
            'fadeIn': fadeIn,
            'fadeOut': fadeOut
        }

        return this._animationService;
    }

    //Declare modal service
    const modalService = function(forum: Commons.Forum.Service, animations: Commons.Animation.Service) {
        if (this._modalService) {
            return this._modalService;
        }

        //List of modals
        let _list: HTMLElement[] = [];

        //Modal structure
        const html: string = `<div id="cs-modal-{{counter}}" class="cs-modal{{class}}"{{data}} style="display:none">
                        <div class="cs-modal-dialog" style="display: none">
                            <div class="cs-modal-content">
                                <div class="cs-modal-head"><div class="cs-modal-title">{{title}}</div><div class="cs-modal-close">X</div></div>
                                <div class="cs-modal-body">{{content}}</div>
                                <div class="cs-modal-footer">{{footer}}</div>
                            </div>
                        </div>
                    </div>`.replace(/(\t|\r?\n|\r)+|\s{2,}/g, '');

        /**
         * Update modal content
         *
         * @param {number} id
         * @param {object} options
         */
        const update = (id: number = 0, options: {title?: string, content?: string, footer?: string} = {}): void => {
            if (typeof _list[id-1] === 'undefined') {
                console.warn('[Commons] Error: modal not found while setting modal content');
                return;
            }

            const currentModal: HTMLElement = _list[id-1];

            if (typeof options.title !== 'undefined') {
                (currentModal.querySelector('.cs-modal-dialog > .cs-modal-content > .cs-modal-head > .cs-modal-title') as HTMLElement).innerHTML = options.title;
            }
            if (typeof options.content !== 'undefined') {
                (currentModal.querySelector('.cs-modal-dialog > .cs-modal-content > .cs-modal-body') as HTMLElement).innerHTML = options.content;
            }
            if (typeof options.footer !== 'undefined') {
                (currentModal.querySelector('.cs-modal-dialog > .cs-modal-content > .cs-modal-footer') as HTMLElement).innerHTML = options.footer;
            }
        }

        /**
         * Create new modal
         *
         * @param {object} options
         * @param {boolean} show
         */
        const set = (options: {class?: string | string[], data?: string | string[], title?: string, content?: string, footer?: string} = {}, show: boolean = false): number => {

            //Check if another modal is currently fading
            if (document.querySelector('.cs-modal-fading')) {
                console.warn('[Commons] Error: modal currently fading while setting new modal');
                return -1;
            }

            //Counter starts at 1
            const counter: number = _list.length + 1;

            //Set replacer
            const replacer: {[key:string]: string} = {
                'class': typeof options.class !== 'undefined' ? (' ' + (options.class instanceof Array ? options.class.join(' ') : options.class)) : '',
                'data': typeof options.data !== 'undefined' ? (' ' + (options.data instanceof Array ? options.data.join(' ') : options.data)) : '',
                'title': typeof options.title !== 'undefined' ? options.title : '',
                'content': typeof options.content !== 'undefined' ? options.content : '',
                'footer': typeof options.footer !== 'undefined' ? options.footer : '',
                'counter': counter.toString()
            };

            //Set regex
            const reg: RegExp = new RegExp('{{(' + Object.keys(replacer).join('\|') + ')}}', 'gi');

            //Append new modal
            document.body.insertAdjacentHTML('beforeend', html.replace(reg, (m, s) => {
                return replacer[s];
            }));

            //Save modal in var and _list
            const current: HTMLElement = document.querySelector('#cs-modal-' + counter) as HTMLElement;
            _list.push(current);

            //Attach default events
            current.addEventListener('click', (e: MouseEvent) => {
                let content = current.querySelector('.cs-modal-content') as HTMLElement;
                if (!content.contains(<Node>e.target)){
                    toggle(counter);
                }
            });
            (current.querySelector('.cs-modal-close') as HTMLElement).addEventListener('click', () => {
                toggle(counter);
            });

            //If show is true, show modal
            if (show) {
                toggle(counter);
            }

            return counter;
        }

        /**
         * Toggle modal
         *
         * @param {number} id
         * @param {?function} callback
         */
        const toggle = (id: number = 0, callback: (() => any) | null = null): void => {
            if (typeof _list[id-1] === 'undefined') {
                console.warn('[Commons] Error: modal not found while toggling');
                return;
            }

            const currentModal: HTMLElement = _list[id-1];

            if (currentModal.style.display === 'block') {
                close(currentModal, callback);
            } else {
                open(currentModal, callback);
            }
        }

        /**
         * Open modal
         *
         * @param {HTMLElement} modal
         * @param {?function} callback
         */
        const open = (modal: HTMLElement | null = null, callback: (() => any) | null = null): void => {
            if (modal === null) {
                console.warn('[Commons] Error: modal not found while opening');
                return;
            }
            if (document.querySelector('.cs-modal-fading')) {
                console.warn('[Commons] Error: modal currently fading while opening another one');
                return;
            }

            const openedModal: HTMLElement | null = document.querySelector('.cs-modal-visible');

            if (openedModal) {
                if (openedModal.id !== modal.id) {
                    close (openedModal, () => {
                        open(modal, callback);
                    });
                }
                return;
            }

            document.body.classList.add('cs-modal-open');
            if ((document.body.scrollHeight > window.innerHeight) && !forum.isFFMobile) {
                document.body.classList.add('cs-modal-scrollwidth');
            }

            modal.classList.add('cs-modal-fading');

            animations.fadeIn(modal, 200, 'block', () => {
                modal.classList.add('cs-modal-fadein');

                animations.fadeIn(modal.querySelector('.cs-modal-dialog') as HTMLElement, 200, 'flex', () => {
                    modal.classList.add('cs-modal-visible');
                    modal.classList.remove('cs-modal-fading', 'cs-modal-fadein');
                    if (callback) {
                        callback();
                    }
                });
            });
        }

        /**
         * Close modal
         *
         * @param {HTMLElement} modal
         * @param {?function} callback
         */
        const close = (modal: HTMLElement | null = null, callback: (() => any) | null = null): void => {
            if (modal === null) {
                const openedModal: HTMLElement | null = document.querySelector('.cs-modal-visible');
                if (openedModal !== null) {
                    modal = openedModal;
                } else {
                    console.warn('[Commons] Error: modal not found while closing');
                    return;
                }
            }

            if (document.querySelector('.cs-modal-fading') !== null) {
                console.warn('[Commons] Error: modal currently fading while closing another one');
                return;
            }

            modal.classList.add('cs-modal-fading', 'cs-modal-fadeout');

            modal.classList.remove('cs-modal-visible');
            animations.fadeOut(modal.querySelector('.cs-modal-dialog') as HTMLElement, 200, () => {
                animations.fadeOut(modal as HTMLElement, 200, () => {
                    document.body.classList.remove('cs-modal-open', 'cs-modal-scrollwidth');
                    (modal as HTMLElement).classList.remove('cs-modal-fading', 'cs-modal-fadeout');
                    if (callback) {
                        callback();
                    }
                });
            });
        }

        this._modalService = {
            'update': update,
            'set': set,
            'toggle': toggle,
            'open': open,
            'close': close
        };

        return this._modalService;
    }

    //Declare toast service
    const toastService = function(device: Commons.Device.Service): Commons.Toast.Service {
        if (this._toastService) {
            return this._toastService;
        }

        let _animateIn: boolean = true, _animateOut: boolean = true,
            _fadeOutDuration: number, _fadeInDuration: number, _leftPosition: string,
            _list: {[key: string]: any}[] = [],
            _animateOutTimeouts: {[key: string]: any} = {};

        //If device is touch
        if (device.isTouch) {
            _fadeOutDuration = _fadeInDuration = 200;
            _leftPosition = `calc(50% - ${document.documentElement.clientWidth > 400 ? '200px' : '48vw'})`;
        } else {
            _fadeOutDuration = 300;
            _fadeInDuration = 600;
        }

        /**
         * Hide a toast
         *
         * @param {string} id
         *
         * @returns {Promise<string | void>}
         */
        const hide = async (id: string): Promise<string | void> => {
            const currentToast: HTMLElement | null = document.querySelector("#" + id),
                alerts: HTMLElement = document.querySelector('#cs-toast-container') as HTMLElement;

            if (_animateOut) {
                _animateOut = false;

                if (currentToast === null) {
                    _animateOut = true;
                    return;
                }

                currentToast.classList.add('slide-out');

                //Device is touch and there is more than one toast active and last toast is disappearing
                if (device.isTouch) {
                    //Clear timeout
                    _list[_list.length-1].schedule.clear();

                    if (alerts.childElementCount > 1 && (alerts.querySelector('.cs-toast:nth-last-child(1)') as HTMLElement).id === id) {

                        let firstToast: HTMLElement = alerts.querySelector('.cs-toast:nth-last-child(2)') as HTMLElement;
                        firstToast.style.maxHeight = 'none';

                        if (alerts.childElementCount > 2) {
                            alerts.querySelectorAll('.cs-toast:not(:nth-last-child(2)):not(:nth-last-child(1))').forEach((toast: HTMLElement) => {
                                toast.style.maxHeight = firstToast.offsetHeight + 'px';
                            })
                        }
                    }
                } else {
                    //Clear timeout - this will prevent timeout to be executed when toast is manually closed
                    window.clearTimeout(_animateOutTimeouts[id]);
                    delete _animateOutTimeouts[id];
                }

                return new Promise((resolve) => {
                    setTimeout(() => {
                        currentToast.remove();
                        _animateOut = true;

                        if (alerts.childElementCount === 0) {
                            alerts.style.display = 'none';
                        }

                        //Resume previous toast timeout and animation
                        if (device.isTouch) {
                            _list.pop();
                            if (_list.length > 0) {
                                _list[_list.length-1].schedule.resume();
                                _list[_list.length-1].toast.querySelector('.cs-toast-timer').style.animationPlayState = 'running';
                            }
                        }
                        resolve('hidden');
                    }, _fadeOutDuration);

                });
            } else {
                await new Promise<void>((resolve) => setTimeout(() => {
                    resolve();
                }, 100));
                return hide(id);
            }
        }

        /**
         * Create and show a toast
         *
         * @param {Object} toast
         *
         * @returns {Promise<string>}
         */
        const show = async (toast?: {id?: string, class?: string | string[], title?: string, content?: string, ttl?: number}): Promise<string> => {
            const alerts: HTMLElement = document.querySelector('#cs-toast-container') as HTMLElement;

            if (alerts.childElementCount < 5) {
                if (!_animateIn) {
                    await new Promise<void>((resolve) => setTimeout(() => {
                        resolve();
                    }, 100));
                    return show(toast);
                } else {
                    _animateIn = false;

                    if (typeof toast === 'undefined') toast = {};
                    if (typeof toast.class === 'undefined') toast.class = ['cs-toast-info'];
                    if (typeof toast.title === 'undefined') toast.title = 'Nuova notifica';
                    if (typeof toast.content === 'undefined') toast.content = '---';
                    if (typeof toast.ttl === 'undefined' || isNaN(Number(toast.ttl)) || toast.ttl < 1000) toast.ttl = 5000;

                    if (typeof toast.id === 'undefined') {
                        do {
                            toast.id = 'cs-toast-' + (Math.floor((Math.random() * 10000000000) + 1000000000));
                        } while (document.querySelector("#" + toast.id) !== null);
                    } else {
                        if (document.querySelector("#" + toast.id) !== null) {
                            await hide(toast.id);
                        }
                    }

                    alerts.style.display = 'block';

                    const div = document.createElement('div');
                    div.innerHTML = `
                        <div class="cs-toast slide-in" id="${toast.id}"> 
                            <div class="cs-toast-timer">
                                <div class="cs-toast-icon"></div>
                            </div>
                            <div class="cs-toast-content">
                                <div class="cs-toast-header">
                                    <div class="cs-toast-title">${toast.title}</div> 
                                    <div class="cs-toast-close">
                                        <button type="button" onclick="window.Commons.toast.hide('${toast.id}');">X</button>
                                    </div>
                                </div> 
                                <div class="cs-toast-body">${toast.content}</div>
                            </div>
                        </div>
                        `.replace(/(\t|\r?\n|\r)+|\s{2,}/g, '');

                    const toastBox: HTMLElement = div.querySelector('div.cs-toast') as HTMLElement;

                    const defaultClasses: string[] = ['cs-toast-info', 'cs-toast-notification', 'cs-toast-error', 'cs-toast-warning', 'cs-toast-success'];

                    if (typeof toast.class === 'string') {
                        toast.class = toast.class.split(' ');
                    }

                    if (!toast.class.some((elm) => {return defaultClasses.indexOf(elm) > -1;})) {
                        toast.class.push('cs-toast-info');
                    }

                    toast.class.forEach(item => {
                        toastBox.classList.add(item);
                    });

                    //Slide in animation
                    toastBox.classList.add('slide-in');

                    //Bg animation duration
                    (toastBox.querySelector('.cs-toast-timer') as HTMLElement).style.animationDuration = `${Math.round(((toast.ttl + _fadeInDuration) / 1000 + Number.EPSILON) * 100) / 100}s`;

                    //Device is touch
                    if (device.isTouch) {

                        toastBox.style.left = _leftPosition;
                        alerts.insertBefore(toastBox, null);

                        //Track touch on toast
                        let touchstartY: number = 0, touchendY: number = 0;
                        toastBox.addEventListener('touchstart', (e: TouchEvent) => {
                            touchstartY = e.changedTouches[0].screenY;
                        });

                        toastBox.addEventListener('touchmove', (e: TouchEvent) => {
                            e.preventDefault();
                            e.stopPropagation();
                        });

                        toastBox.addEventListener('touchend', (e: TouchEvent) => {
                            touchendY = e.changedTouches[0].screenY;
                            if (touchstartY - touchendY > 10) {
                                (toastBox.querySelector('.cs-toast-close button') as HTMLElement).dispatchEvent(new Event('click'));
                            }
                        });

                        setTimeout(() => {
                            //Set max height to prevent overflow
                            alerts.querySelectorAll('.cs-toast:not(:last-of-type)').forEach((toast: HTMLElement) => {
                                toast.style.maxHeight = toastBox.offsetHeight + 'px';
                            });
                        }, _fadeInDuration * 2 / 3);

                        //Pause last toast
                        if (_list.length > 0) {
                            _list[_list.length - 1].schedule.pause();
                            _list[_list.length - 1].toast.querySelector('.cs-toast-timer').style.animationPlayState = 'paused';
                        }

                        //Add toast to list
                        _list.push({
                            'schedule': new _schedule(() => {
                                hide((toast as HTMLElement).id);
                            }, _fadeInDuration + toast.ttl),
                            'toast': toastBox
                        });
                    } else {
                        //Device is not touch
                        alerts.insertBefore(toastBox, alerts.childNodes[0]);
                        _animateOutTimeouts[toast.id] = setTimeout(() => {
                            hide((toast as HTMLElement).id);
                        }, _fadeInDuration + toast.ttl);
                    }

                    return new Promise((resolve) => {
                        setTimeout(() => {
                            _animateIn = true;
                            resolve('shown');
                        }, _fadeInDuration);
                    });
                }
            } else {
                await new Promise<void>((resolve) => setTimeout(() => {
                    resolve();
                }, 500));
                return show(toast);
            }
        }

        /**
         * Create a stoppable timer
         *
         * @param {function} callback
         * @param {number} delay
         */
        const _schedule: any = function(callback: () => any, delay: number) {
            let id: number, start: number, remaining = delay;

            this.pause = function() {
                window.clearTimeout(id);
                remaining -= Date.now() - start;
            };

            this.resume = function() {
                start = Date.now();
                window.clearTimeout(id);
                id = window.setTimeout(callback, remaining);
            };

            this.clear = function() {
                window.clearTimeout(id);
            };

            this.resume();
        };


        this._toastService = {
            'show': show,
            'hide': hide
        };

        return this._toastService;
    }

    if(typeof window.Commons === 'undefined') {

        console.log('Init commons!');

        //Append styles if needed
        if (document.querySelector('#commonslib-css') === null) {
            let style: HTMLStyleElement = document.createElement('style');
            style.setAttribute('id', 'commonslib-css');
            style.appendChild(document.createTextNode(`#cs-toast-container{display:none;z-index:1001;position:fixed;right:25px;height:auto;box-sizing:border-box;max-width:calc(100% - 50px)}#cs-toast-container:not(.cs-touch-device){top:100px;width:400px}#cs-toast-container.cs-touch-device{width:calc(100% - 50px);top:10px;text-align:center}.cs-toast{opacity:0;max-width:400px;min-height:70px;overflow:hidden;box-shadow:0 .25rem .75rem rgba(0,0,0,.1);border-radius:.25rem;margin-bottom:.75rem;background:#fff;display:flex;flex-direction:row}.cs-touch-device .cs-toast{width:96vw;position:absolute}.cs-toast-timer{width:40px;margin:0;align-self:stretch;position:relative;border-right:1px solid #b7b7b7;background-position:bottom;background-size:100% 200%;animation-name:cs-toast-bg;animation-timing-function:linear;animation-fill-mode:forwards}.cs-toast-content{margin:0;width:calc(100% - 41px)}.cs-toast:not(.cs-toast-dark) .cs-toast-content{background:#fff}.cs-toast.cs-toast-dark{border:1px solid #b7b7b7}.cs-toast.cs-toast-dark .cs-toast-content{background:#1f1f1f}.cs-toast-header{display:flex;flex-direction:row;align-items:center;padding:.5rem .75rem;font-size:1rem;border-bottom:1px solid #b7b7b7;margin:0}.cs-toast-title{margin:0 auto 0 0}.cs-toast-close{margin:0}#cs-toast-container:not(.cs-touch-device) .cs-toast-close{opacity:.8;transition:opacity .2s linear}#cs-toast-container:not(.cs-touch-device) .cs-toast-close:hover{opacity:1}.cs-toast-close button{margin:0;padding:0;font-family:inherit;font-size:inherit;line-height:inherit;border:0;background:0 0;outline:0;color:inherit}.cs-toast-icon{position:absolute;top:17px;left:calc(50% - 13px);width:26px;height:26px;border-radius:50%;background-repeat:no-repeat;background-position:50% 50%}.cs-toast-body{margin:0;padding:.5rem .75rem;font-size:.875rem;text-align:left}.cs-touch-device .cs-toast:nth-child(1){top:0}.cs-touch-device .cs-toast:nth-child(2){top:10px}.cs-touch-device .cs-toast:nth-child(3){top:20px}.cs-touch-device .cs-toast:nth-child(4){top:30px}.cs-touch-device .cs-toast:nth-child(5){top:40px}.cs-touch-device .cs-toast.slide-in{animation:cs-toast-touch-slidein .2s linear forwards}.cs-touch-device .cs-toast.slide-out{animation:cs-toast-touch-slideout .2s linear forwards}#cs-toast-container:not(.cs-touch-device) .cs-toast.slide-in{animation:cs-toast-slidein .6s cubic-bezier(.1,1.06,.6,1.34) forwards}#cs-toast-container:not(.cs-touch-device) .cs-toast.slide-out{animation:cs-toast-slideout .3s linear forwards}@keyframes cs-toast-slidein{from{transform:translateX(50%);opacity:0}to{transform:translatex(0);opacity:1}}@keyframes cs-toast-slideout{from{opacity:1}to{opacity:0}}@keyframes cs-toast-touch-slidein{from{transform:translateY(-75%);opacity:0}to{transform:translateY(0);opacity:1}}@keyframes cs-toast-touch-slideout{from{transform:translateY(0);opacity:1}to{transform:translateY(-75%);opacity:0}}@keyframes cs-toast-bg{from{background-position:bottom}to{background-position:top}}.cs-toast-info .cs-toast-timer,.cs-toast-notification .cs-toast-timer{background-color:#2681f9;background-image:linear-gradient(to bottom,#b1d3fd 50%,#2681f9 50%)}.cs-toast-info:not(.cs-toast-dark) .cs-toast-content,.cs-toast-notification:not(.cs-toast-dark) .cs-toast-content{color:#132440}.cs-toast-info.cs-toast-dark .cs-toast-content,.cs-toast-notification.cs-toast-dark .cs-toast-content{color:#e7f0ff}.cs-toast-info .cs-toast-icon{background-color:#ebf4ff;background-image:url(https://upload.forumfree.net/i/ff11674905/icons/toast-info.svg);background-size:63%}.cs-toast-success .cs-toast-timer{background-color:#0d8a39;background-image:linear-gradient(to bottom,#64ce89 50%,#0d8a39 50%)}.cs-toast-success:not(.cs-toast-dark) .cs-toast-content{color:#155724}.cs-toast-success.cs-toast-dark .cs-toast-content{color:#ecfff1}.cs-toast-success .cs-toast-icon{background-color:#ecf7eb;background-image:url(https://upload.forumfree.net/i/ff11674905/icons/toast-check.svg);background-size:56%}.cs-toast-error .cs-toast-timer{background-color:#d81425;background-image:linear-gradient(to bottom,#ffa7af 50%,#d81425 50%)}.cs-toast-error:not(.cs-toast-dark) .cs-toast-content{color:#721c24}.cs-toast-error.cs-toast-dark .cs-toast-content{color:#ffedef}.cs-toast-error .cs-toast-icon{background-color:#ffe6e9;background-image:url(https://upload.forumfree.net/i/ff11674905/icons/toast-error1.svg);background-size:48%}.cs-toast-warning .cs-toast-timer{background-color:#e69c14;background-image:linear-gradient(to bottom,#fbbd4b 50%,#e69c14 50%)}.cs-toast-warning:not(.cs-toast-dark) .cs-toast-content{color:#5d4500}.cs-toast-warning.cs-toast-dark .cs-toast-content{color:#fff3d0}.cs-toast-warning .cs-toast-icon{background-color:#ffe6b6;background-image:url(https://upload.forumfree.net/i/ff11674905/icons/toast-warning.svg);background-size:63%}.cs-toast-notification .cs-toast-icon{background-color:#ebf4ff;background-image:url(https://upload.forumfree.net/i/ff11674905/icons/toast-bell1.svg);background-size:53%}.cs-modal{display:none;position:fixed;height:100%;top:0;left:0;z-index:1000;background:rgba(0,0,0,.5)}.cs-modal,.cs-modal-content{width:100%}.cs-modal-content,.cs-modal-dialog,.cs-modal-footer,.cs-modal-head{display:flex}.cs-modal-content{flex-direction:column;position:relative;border-radius:.25rem;max-height:calc(100vh - 1rem)}.cs-modal:not(.cs-modal-dark) .cs-modal-content{background:#fff;color:#585858}.cs-modal.cs-modal-dark .cs-modal-content{background:#1f1f1f;color:#eaeaea;border:1px solid #b7b7b7}.cs-modal-head{align-items:center;justify-content:center;flex-direction:row}.cs-modal-footer,.cs-modal-head{flex-shrink:0}.cs-modal-body{flex:1 1 auto}body.cs-modal-open{overflow:hidden}body.cs-modal-scrollwidth{padding-right:17px}.cs-modal,.cs-modal-body,.cs-modal-footer,.cs-modal-head{overflow-y:auto}.cs-modal-body{overflow-x:hidden}.cs-modal,.cs-modal *{box-sizing:border-box;margin:0;line-height:initial}.cs-modal:not(.cs-modal-font-auto),.cs-modal:not(.cs-modal-font-auto) *{font-size:1rem}.cs-modal .cs-modal-close{cursor:pointer;margin-left:auto}.cs-modal-dialog{margin:.5rem;max-height:calc(100% - 1rem)}.cs-modal:not(.cs-modal-visible) .cs-modal-dialog{transform:translate3d(0,-50px,0)}.cs-modal.cs-modal-fadein .cs-modal-dialog{animation:cs-modal-fadein-anim .2s linear forwards}.cs-modal.cs-modal-fadeout .cs-modal-dialog{animation:cs-modal-fadeout-anim .2s linear forwards}.cs-modal-content>*{padding:1rem}.cs-modal-bodypadding-0 .cs-modal-body{padding:0!important}.cs-modal-content>:not(:last-child){border-bottom:1px solid #b7b7b7}.cs-modal .cs-modal-title,.cs-modal .cs-modal-title *{font-size:1.5rem}.cs-modal-h10 .cs-modal-dialog{height:10%}.cs-modal-h20 .cs-modal-dialog{height:20%}.cs-modal-h30 .cs-modal-dialog{height:30%}.cs-modal-h40 .cs-modal-dialog{height:40%}.cs-modal-h50 .cs-modal-dialog{height:50%}.cs-modal-h60 .cs-modal-dialog{height:60%}.cs-modal-h70 .cs-modal-dialog{height:70%}.cs-modal-h80 .cs-modal-dialog{height:80%}.cs-modal-h90 .cs-modal-dialog{height:90%}.cs-modal-h100 .cs-modal-dialog{height:100%}.cs-modal-text-left .cs-modal-body{text-align:left}.cs-modal-text-left .cs-modal-footer{text-align:left;justify-content:flex-start}.cs-modal-text-right .cs-modal-body{text-align:right}.cs-modal-text-right .cs-modal-footer{text-align:right;justify-content:flex-end}.cs-modal-text-center .cs-modal-body{text-align:center}.cs-modal-text-center .cs-modal-footer{text-align:center;justify-content:center}@keyframes cs-modal-fadein-anim{from{transform:translate3d(0,-50px,0)}to{transform:translate3d(0,0,0)}}@keyframes cs-modal-fadeout-anim{from{transform:translate3d(0,0,0)}to{transform:translate3d(0,-50px,0)}}@media only screen and (min-width:992px){.cs-modal-dialog{margin:2rem auto;max-height:calc(100% - 4rem);max-width:600px}.cs-modal-content{max-height:calc(100vh - 4rem)}.cs-modal[class*=" cs-modal-w"] .cs-modal-dialog{max-width:initial}.cs-modal-w10 .cs-modal-dialog{width:10%}.cs-modal-w20 .cs-modal-dialog{width:20%}.cs-modal-w30 .cs-modal-dialog{width:30%}.cs-modal-w40 .cs-modal-dialog{width:40%}.cs-modal-w50 .cs-modal-dialog{width:50%}.cs-modal-w60 .cs-modal-dialog{width:60%}.cs-modal-w70 .cs-modal-dialog{width:70%}.cs-modal-w80 .cs-modal-dialog{width:80%}.cs-modal-w90 .cs-modal-dialog{width:90%}.cs-modal-w100 .cs-modal-dialog{width:100%}.cs-modal-h100 .cs-modal-dialog{max-height:100%;margin:0 auto}.cs-modal-h100 .cs-modal-content{max-height:100%}}.cs-buttons{display:flex;align-items:center;justify-content:center;flex-direction:row;flex-wrap:wrap;width:100%}.cs-buttons.cs-buttons-right{justify-content:flex-end}.cs-buttons.cs-buttons-left{justify-content:flex-start}.cs-btn{margin:0 .5rem;background:0 0;outline:0!important;font-family:inherit;font-size:inherit;line-height:inherit;padding:.5rem .75rem;border-radius:.25rem;transition:all .15s ease-in-out;border:1px solid transparent}.cs-btn.cs-btn-outer-blue{color:#007bff;border-color:#007bff}.cs-btn.cs-btn-outer-blue:hover{color:#fff;background-color:#007bff;border-color:#007bff}.cs-btn.cs-btn-blue{color:#fff;background-color:#007bff;border-color:#007bff}.cs-btn.cs-btn-blue:hover{background-color:#0069d9;border-color:#0062cc}.cs-btn.cs-btn-outer-green{color:#28a745;border-color:#28a745}.cs-btn.cs-btn-outer-green:hover{color:#fff;background-color:#28a745;border-color:#28a745}.cs-btn.cs-btn-green{color:#fff;background-color:#28a745;border-color:#28a745}.cs-btn.cs-btn-green:hover{color:#fff;background-color:#218838;border-color:#1e7e34}.cs-btn.cs-btn-outer-red{color:#dc3545;border-color:#dc3545}.cs-btn.cs-btn-outer-red:hover{color:#fff;background-color:#dc3545;border-color:#dc3545}.cs-btn.cs-btn-red{color:#fff;background-color:#dc3545;border-color:#dc3545}.cs-btn.cs-btn-red:hover{color:#fff;background-color:#c82333;border-color:#bd2130}.cs-btn.cs-btn-outer-yellow{color:#ffc107;border-color:#ffc107}.cs-btn.cs-btn-outer-yellow:hover{color:#212529;background-color:#ffc107;border-color:#ffc107}.cs-btn.cs-btn-yellow{color:#212529;background-color:#ffc107;border-color:#ffc107}.cs-btn.cs-btn-yellow:hover{color:#212529;background-color:#e0a800;border-color:#d39e00}.cs-btn.cs-btn-outer-light{color:#f8f9fa;border-color:#f8f9fa}.cs-btn.cs-btn-outer-light:hover{color:#212529;background-color:#f8f9fa;border-color:#f8f9fa}.cs-btn.cs-btn-light{color:#212529;background-color:#f8f9fa;border-color:#f8f9fa}.cs-btn.cs-btn-light:hover{color:#212529;background-color:#e2e6ea;border-color:#dae0e5}.cs-btn.cs-btn-outer-dark{color:#343a40;border-color:#343a40}.cs-btn.cs-btn-outer-dark:hover{color:#fff;background-color:#343a40;border-color:#343a40}.cs-btn.cs-btn-dark{color:#fff;background-color:#343a40;border-color:#343a40}.cs-btn.cs-btn-dark:hover{color:#fff;background-color:#23272b;border-color:#1d2124}.cs-btn.cs-btn-outer-gray{color:#6c757d;border-color:#6c757d}.cs-btn.cs-btn-outer-gray:hover{color:#fff;background-color:#6c757d;border-color:#6c757d}.cs-btn.cs-btn-gray{color:#fff;background-color:#6c757d;border-color:#6c757d}.cs-btn.cs-btn-gray:hover{background-color:#5a6268;border-color:#545b62}`));
            document.head.appendChild(style);
        }

        try {
            const forum = forumService(),
                device = deviceService();

            (window as Window).Commons = {
                'location': locationService(forumService(), utilityService()),
                'forum': forum,
                'user': userService(forum),
                'device': device,
                'utilities': utilityService(),
                'animations': animationService(),
                'modal': modalService(forumService(), animationService()),
                'toast': toastService(device)
            };


            let domStateCheck = setInterval(() => {
                console.log('Commons interval!');

                if(forum.isFFMobile) {
                    if (document.querySelector('aside#Left')) {
                        clearInterval(domStateCheck);
                        (document.querySelector('body > *:first-child') as HTMLElement).insertAdjacentHTML('afterbegin', `<div id="cs-toast-container"${device.isTouch ? ' class="cs-touch-device"' : ''}></div>`)
                    }
                }
                else if (document.querySelector('#ffHtmlTopStart')) {
                    clearInterval(domStateCheck);
                    (document.querySelector('#ffHtmlTopStart') as HTMLElement).insertAdjacentHTML('beforebegin', `<div id="cs-toast-container"${device.isTouch ? ' class="cs-touch-device"' : ''}></div>`);
                }

                if (document.readyState !== 'loading') {
                    clearInterval(domStateCheck);
                }

            }, 100);
        }
        catch(e) {
            console.log('Commons error!', e);
        }
    }
})();
