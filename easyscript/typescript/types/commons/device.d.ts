declare namespace Device {
    interface Service {
        /**
         * TRUE if device is touch.
         */
        isTouch: boolean;
    }
}

export default Device;
