declare namespace Modal {
    interface Service {
        /**
         * Close modal.
         *
         * @param {HTMLElement} [modal = null]
         * @param {function} [callback = null]
         */
        close: (modal?: HTMLElement, callback?: () => any) => void;

        /**
         * Open modal.
         *
         * @param {HTMLElement} modal
         * @param {function} [callback = null]
         */
        open: (modal: HTMLElement, callback?: () => any) => void;

        /**
         * Create a new modal.
         *
         * @param {object} [options = {}]
         * @param {boolean} [show = false]
         *
         * @returns {number}
         */
        set: (options?: {class?: string | string[], data?: string | string[], title?: string, content?: string, footer?: string}, show?: boolean) => number;

        /**
         * Toggle modal.
         *
         * @param {number} id
         * @param {function} [callback = null]
         */
        toggle: (id: number, callback?: () => any) => void;

        /**
         * Update modal content.
         *
         * @param {number} id
         * @param {object} [options = {}]
         */
        update: (id: number, options?: {title?: string, content?: string, footer?: string}) => void;
    }
}

export default Modal;
