declare namespace Animation {
    interface Service {
        /**
         * Fade in an element.
         *
         * @param {HTMLElement} element
         * @param {number} [duration = 300] - duration in milliseconds
         * @param {string} [display = block]
         * @param {function} [callback = null]
         */
        fadeIn: (element: HTMLElement, duration?: number, display?: string, callback?: () => any) => void;

        /**
         * Fade out an element.
         *
         * @param {HTMLElement} element
         * @param {number} [duration = 300] - duration in milliseconds
         * @param {function} [callback = null]
         */
        fadeOut: (element: HTMLElement, duration?: number, callback?: () => any) => void;
    }
}

export default Animation;
