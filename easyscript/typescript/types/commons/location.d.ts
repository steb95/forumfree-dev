declare namespace Location {
    interface Service {
        /**
         * TRUE if current topic is a blog article.
         */
        isArticle: boolean;

        /**
         * TRUE if current page is a list of articles.
         */
        isArticleList: boolean;

        /**
         * TRUE if current page is blog.
         */
        isBlog: boolean;

        /**
         * TRUE if current page is full-editor reply page.
         */
        isFullEditor: boolean;

        /**
         * TRUE if current page is homepage (blog/forum).
         */
        isHome: boolean;

        /**
         * TRUE if current page is a user's profile.
         */
        isProfile: boolean;

        /**
         * TRUE if current page is section.
         */
        isSection: boolean;

        /**
         * TRUE if current page is topic.
         */
        isTopic: boolean;

        /**
         * Section info.
         */
        section: {
            /**
             * Get section id.
             * 0 if not a section or id is not available.
             */
            id: number;

            /**
             * Get section title.
             * NULL if not a section or title is not available.
             */
            title: string | null;
        };

        /**
         * Topic info.
         */
        topic: {
            /**
             * Get topic id.
             * 0 if not a topic or id is not available.
             */
            id: number;

            /**
             * Get topic title.
             * NULL if not a topic or title is not available.
             */
            title: string | null;
        };
    }
}

export default Location;
