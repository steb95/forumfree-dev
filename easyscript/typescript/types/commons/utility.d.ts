declare namespace Utility {
    interface Service {
        /**
         * Init a BBCode parser.
         */
        bbcodeParser: () => void;

        /**
         * Format a date following input replacer.
         *
         * @param {number | string} value
         * @param {string} [format = Y-m-d H:i:s]
         *
         * @returns {string | null}
         */
        dateFormat: (value: number | string, format?: string) => string | null;

        /**
         * Get all cookies as key => value object.
         *
         * @returns {object}
         */
        getAllCookies: () => object;

        /**
         * Get cookie from name.
         *
         * @param {string} name
         *
         * @returns {string | null}
         */
        getCookie: (name: string) => string | null;

        /**
         * Get parameter from url.
         *
         * @param {string} name
         * @param {string | null} [url = null]
         *
         * @returns {string | null}
         */
        getUrlParameter: (name: string, url?: string | null) => string | null;

        /**
         * Remove js in tags (e.g. onclick=).
         *
         * @param {string} text
         *
         * @returns {string}
         */
        removeJsInTags: (text: string) => string;

        /**
         * Remove tags from string.
         *
         * @param {string} text
         * @param {string[]} [tags = [script, style]]
         *
         * @returns {string}
         */
        removeTags: (text: string, tags?: string[]) => string;

        /**
         * Set a new cookie.
         *
         * @param {string} name
         * @param {string} value
         * @param {number} [seconds = null]
         * @param {boolean} [network = false]
         *
         * @returns {boolean}
         */
        setCookie: (name: string, value: string, seconds?: number, network?: boolean) => boolean;

        /**
         * Filter input array and return items that have distinct value of the key property.
         *
         * @param {object[]} array
         * @param {string} key
         *
         * @returns {object[]}
         */
        uniqueItems: (array: object[], key: string) => any[];
    }
}

export default Utility;
