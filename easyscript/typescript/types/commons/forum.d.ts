declare namespace Forum {
    interface Service {
        /**
         * Get forum id, that is forumfree CID.
         */
        id: number;

        /**
         * TRUE if forum layout is FFMobile.
         */
        isFFMobile: boolean;

        /**
         * TRUE if forum layout is Quirks.
         */
        isQuirks: boolean;

        /**
         * TRUE if forum layout is Responsive.
         */
        isResponsive: boolean;

        /**
         * TRUE if forum layout is Standard.
         */
        isStandard: boolean;

        /**
         * Get a number that stands for forum layout.
         * 0 - FFMobile;
         * 1 - Quirks;
         * 2 - Standard;
         * 3 - Responsive.
         */
        layout: number;
    }
}

export default Forum
