declare namespace User {
    interface Service {
        /**
         * Get user avatar url.
         */
        avatar: string | null;

        /**
         * Get user group.
         * -1 if it's guest.
         * For admin and moderators without a custom group,
         * the property is set to admin and mod_sez respectively.
         */
        group: string | number;

        /**
         * Get user id.
         */
        id: number;

        /**
         * TRUE if user is admin.
         */
        isAdmin: boolean;

        /**
         * TRUE if user is global mod.
         */
        isGlobalMod: boolean;

        /**
         * TRUE if user is graphic admin.
         */
        isGraphicAdmin: boolean;

        /**
         * TRUE if it's guest.
         */
        isGuest: boolean;

        /**
         * TRUE if user is moderator.
         */
        isMod: boolean;

        /**
         * TRUE if user is script admin.
         */
        isScriptAdmin: boolean;

        /**
         * TRUE if user is section admin.
         */
        isSectionAdmin: boolean;

        /**
         * TRUE if user is user admin.
         */
        isUserAdmin: boolean;

        /**
         * User lang.
         * Default en.
         */
        lang: string;

        /**
         * Get user nickname.
         * NULL if it's guest.
         */
        nickname: string | null;
    }
}

export default User;
