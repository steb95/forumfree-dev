declare namespace Toast {
    interface Service {
        /**
         * Hide a toast.
         *
         * @param {string} id
         *
         * @returns {Promise<string>}
         */
        hide: (id: string) => Promise<string>;

        /**
         * Create and show a new toast.
         *
         * @param {Object} [toast = {}]
         *
         * @returns {Promise<string>}
         */
        show: (toast?: {id?: string, class?: string | string[], title?: string, content?: string, ttl?: number}) => Promise<string>;
    }
}

export default Toast;
