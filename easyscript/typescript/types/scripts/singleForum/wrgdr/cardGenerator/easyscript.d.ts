declare namespace Easyscript {
    interface Options {
        html: string;
        section: number;
        selector: string;
    }
}

export default Easyscript;
