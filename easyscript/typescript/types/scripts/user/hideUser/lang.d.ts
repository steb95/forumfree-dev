declare namespace Lang {
    interface Elements {
        post: string;
        quote: string;
        topic: string;
        message: string;
        author: string;
    }
}

export default Lang;
