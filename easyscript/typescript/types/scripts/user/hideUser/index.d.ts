export as namespace HideUser;

import Cache from "./cache";
import Easyscript from "./easyscript";
import Lang from "./lang";

export { Cache, Easyscript, Lang };
