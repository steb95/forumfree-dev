declare namespace Easyscript {
    interface Options {
        postMsg: string;
        topicMsg: string;
        tagMsg: string;
        quoteMsg: string;
        accounts: {
            id: string;
            seeAuthor: boolean;
            hideTopics: boolean;
            hideTags: boolean;
        }[];
    }
}

export default Easyscript;
