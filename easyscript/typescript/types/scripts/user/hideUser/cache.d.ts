declare namespace Cache {
    interface Storage {
        v: string;
        exp: number;
        users: {[key: string]: User};
    }

    interface User {
        id: number;
        name: string;
    }
}

export default Cache;
