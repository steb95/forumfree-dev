import Easyscript from "./easyscript";
import Post from "./post";

interface Items {
    postInfo: Post.Info;
    hasPost: boolean;
    forumList: string[];
    cursorStartPosition: number;
    cursorEndPosition: number;
}

declare namespace Sentence {
    type Info = Items & Easyscript.OptionList
}

export default Sentence;
