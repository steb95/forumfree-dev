declare namespace Post {
    interface Info {
        content: string;
        id: number;
        sent: boolean;
        subdomain: string;
    }
}

export default Post;
