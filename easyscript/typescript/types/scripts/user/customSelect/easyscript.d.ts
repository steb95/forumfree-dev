declare namespace Easyscript {
    interface Options {
        optionList: OptionList[];
        selectTitle: string;
    }
    interface OptionList {
        dice: boolean;
        forum: string|undefined;
        post: string|undefined;
        title: string;
        value: string;
    }
}

export default Easyscript;
