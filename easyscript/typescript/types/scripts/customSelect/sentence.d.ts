import Easyscript from "./easyscript";

interface Items {
    cursorStartPosition: number;
    cursorEndPosition: number;
}

declare namespace Sentence {
    type Info = Items & Easyscript.OptionList
}

export default Sentence;
