export as namespace CustomSelect;

import Sentence from "./sentence";
import Easyscript from "./easyscript";

export { Easyscript, Sentence };
