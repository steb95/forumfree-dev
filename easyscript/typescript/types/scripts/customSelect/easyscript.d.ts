declare namespace Easyscript {
    interface Options {
        optionList: OptionList[];
        selectTitle: string;
    }
    interface OptionList {
        adminOnly: boolean;
        customGroupId: string;
        customUserId: string;
        dice: boolean;
        secBlacklist: string;
        secWhitelist: string;
        title: string;
        topicBlacklist: string;
        topicWhitelist: string;
        value: string;
    }
}

export default Easyscript;
