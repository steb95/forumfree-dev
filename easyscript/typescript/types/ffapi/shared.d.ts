declare namespace Shared {
     type IdForum = {
         idForum: number;
     };

     type User = {
         id: number;
         name: string;
         avatar: string;
     };

     type Group = {
         id: number;
         name: string;
         class: string;
         bodyclass: string;
     };
}

export default Shared;
