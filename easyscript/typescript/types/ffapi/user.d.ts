import Shared from "./shared";

type UserKeys = string;

interface UserProperties {
    banned: 0 | 1;
    bday_day?: string;
    bday_month?: string;
    bday_year?: string;
    blocked?: number;
    circuito: "bf" | "fc" | "ff";
    contacts?: string;
    domain_last?: string;
    easyscript: string;
    enable_sign: string;
    enable_subs: string;
    friends?: number[];
    gender: "" | "f" | "m" | "n";
    group: Shared.Group;
    hide_email?: string;
    interests?: string;
    language: string;
    lastVisit: string;
    lastActivity: string;
    messages: number;
    name_last?: string;
    nobody_can_contact?: 0 | 1;
    origin?: string;
    permission: {
        admin: 0 | 1;
        admin_graphic: 0 | 1;
        admin_sez: 0 | 1;
        admin_users: 0 | 1;
        ffstaff?: 0 | 1;
        founder: 0 | 1;
        global_mod: 0 | 1;
        mod_sez: 0 | 1;
    };
    registration: string;
    signature?: string;
    site_freq?: number[];
    site_last?: number;
    status: "anonimo" | "offline" | "online";
    title?: string;
    view_avs: string;
    view_img: string;
    website?: string;
}

declare namespace User {
    type Response = Shared.IdForum & Record<UserKeys, UserProperties & Shared.User>
}

export default User;
