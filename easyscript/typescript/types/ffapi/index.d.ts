export as namespace FFApi;

import Group from "./group";
import User from "./user";

export { Group, User };
