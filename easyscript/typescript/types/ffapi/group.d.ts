import Shared from "./shared";

type GroupKeys = string;

type PredefinedGroupKeys = "admin" | "members" | "mod" | "validating";

interface GroupProperties {
    info: {
        adm_forum: "" | "1";
        adm_globale: "" | "1";
        adm_grafica:  "" | "1";
        adm_utenti: "" | "1";
        desc: string;
        image: string;
        membership: 0 | 1;
        mod_forums?: {
            id: number;
            name: string;
        }[];
        name: string;
    };
    users?: number[] | Shared.User[];
}

declare namespace Group {
    type Response = Shared.IdForum & Record<PredefinedGroupKeys, Shared.User[] | number[]> & Record<GroupKeys, GroupProperties>;
}

export default Group;
