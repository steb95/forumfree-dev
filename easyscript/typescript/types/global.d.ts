/**
 * Forumfree javascript vars
 */
declare var ff_layout: number | undefined;

declare var ff_cid: number | undefined;

declare var ff_mid: number | undefined;

/**
 * Window extended
 */
declare interface Window {
    /**
     * Commons lib.
     */
    Commons: {
        animations: Commons.Animation.Service;
        device: Commons.Device.Service;
        forum: Commons.Forum.Service;
        location: Commons.Location.Service;
        modal: Commons.Modal.Service;
        toast: Commons.Toast.Service;
        user: Commons.User.Service;
        utilities: Commons.Utility.Service;
    };
    /**
     * Easyscript
     */
    FFScript: {
        scripts_admin: [];
        settings: {
            //Hide User settings
            script107: HideUser.Easyscript.Options;
            //Test script settings
            script120: CustomSelect.Easyscript.Options;
            //Custom Select settings
            script121: CustomSelect.Easyscript.Options;
            //User Custom Select settings
            script122: UserCustomSelect.Easyscript.Options;
            //Test script 2 settings
            script139: wrgdrCardGenerator.Easyscript.Options;
            //WRGDR card generator settings
            script162: wrgdrCardGenerator.Easyscript.Options;
        };
    };
}
